export const PI = Math.PI

export let user = 'Grover'
       let pwd = 'qwerty'
       
// tmb podemos exportar una funcion x defecto, cuando esta sea llamada desde otro archivo con un import
// no se puede tener dos funciones o dos variables por defecto, sino sólo una.
export default function saludar(){
    console.log('Holaaaa +ES6')
}

// si deseamos exportar x default una funcion expresada, ésta no se podrá
// export default const saludo = () => {
//     console.log('Hello world!')
// }

// pero si podremos hacerlo así...
const saludo = () => {
    console.log('Holaaaa...')
}
// export default saludo

// tmb se pueden exportar clases
export class Saludar{
    constructor(){
        console.log('Hola Clases +ES6')    
    }
}