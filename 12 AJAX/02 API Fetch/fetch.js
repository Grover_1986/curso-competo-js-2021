
/******📄 API FETCH 📄******/
//◽ Con Fetch no tenemos q hacer instancia de ningún objeto de AJAX
//◽ Fetch es un mecanismo q trabaja con Promesas
//◽ El primer .then() transforma la respuesta, ya q es un body ReadableStream, lo transforma ya sea a Json o a Texto.
//◽ El segundo .then() nos sirve para hacer la lógica de programación.
//◽ El tercero es el .catch() para q nos arroje algún error si es q lo hay.
//◽ El cuarto es el .finally(), éste es opcional.

((d) => {
    const $fetch = d.getElementById('fetch'),
          $fragment = d.createDocumentFragment()
          
    //👇 Usamos el método Fetch() y como parámetros nos pide la URL, tmb le podemos pasar un parámetro de opciones q sería un Objeto // , pero eso veremos cuando hagamos un CRUD con Fetch, y bueno, ahora por defecto la opción method es GET
    fetch('https://jsonplaceholder.typicode.com/user')
    .then((res) =>{ //👈.then() ejecuta la parte positiva, éste es como si fuera if
        console.log(res) //👈aquí nos devolverá la respuesta como un Objeto q no podemos manejar textualmente
        // Opción 1 👇
        return res.json() //👈esa respuesta tenemos q convetirla a un formato válido, .json() es un método de API Fetch 
        // Opción 2 👇
        return res.text() //👈tmb podemos convertirlo a texto, si estamos recibiendo código Html o Xml 
        // Opción 3 👇  
        return res.blob() //👈pero si estamos recibiendo una imagen en formato dataURI. entonces usamos método blob()
        //☝en console.log(res) a diferencia de XMLHttpRequest, obtenemos un objeto de tipo Response, y el ok es como un ReadyState
        //☝en console.log(res) vemos q la respuesta json viene en el body, pero es un ReadableStream y hay q convertirlo, para eso 
        // crearemos otro .then()
        
        //👇Si quitamos de nuestra URL una letra x ejm 's', el catch() debe arrojarnos un mensaje de error, pero vemos q no lo 
        // hace, eso es xq al igual q en XMLHttpRequest debemos hacer una validación para el Código de Respuesta, para eso usaremos 
        // la propiedad ok del Objeto Response, éste nos devuelve true cuando se ha completado satisfactoriamente una solicitud HTTP
        // y false cuando no se haya completado satisfactoriamente. Utilizaremos un Operador Ternario, si es correcto trae el json,
        // pero sino es correcto llamanos al Objeto Promise y ejecutamos el método reject() para forzar a rechazar la Promesa.
        // Al momento de usar Promise.reject(res) en automatico se lanza en el .catch() el error, siempre y cuando lo haya.
        
        /* Opción 4 👇, la q usaremos, los anteriores return no las usaremos por tanto ni escribirlas */ 
        return res.ok ? res.json() : Promise.reject(res);
        
    })
    .then((json) => {  // la respuesta return res.json(), aquí en este mecanismo .then(), la recibimos como formato json
        console.log(json)
        json.forEach(el =>{
            const $li = d.createElement('li')
            $li.innerHTML = `${el.name} - ${el.email} - ${el.address.city}`
            $fragment.appendChild($li)
        })
        $fetch.appendChild($fragment)
    })
    .catch((err) => {//👈.catch() ejecuta la parte negativa, éste es como si fuera Else, pero nos devuelve mensaje de error
        console.error(err)
        let message = err.statusText || 'Ocurrió un error'
        $fetch.innerHTML = `<p style='color:red;'>Error ${err.status}: ${message}</p>`
    }) 
    .finally(() => {//👈.finally() se ejecuta independientemente del resultado, tmb nos puede servir para poner un loader
        console.log("Esto se ejecutará independientemente del resultado de la Promesa Fetch")
    })  
    
})(document);