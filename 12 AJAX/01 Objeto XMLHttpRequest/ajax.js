/*📄IMPORTANTE:
    * Como AJAX trabaja parte con el Cliente, pero tmb parte se ejecuta en el Servidor, es muy importante que AJAX se ejecute de
    manera local en un Servidor Web usando el Protocolo HTTP, para eso usamos Live Server
*/

/******📄 OBJETO XMLHTTPREQUEST 📄******/

/* Lo que haremos es consumir una API externa.
    * Trabajaremos haciendo peticiones a una API externa falsa para novatos llamada 👉 https://jsonplaceholder.typicode.com 
    * También podremos consumir archivos JSON localmente.
    * Esta es la ruta q vamos a consumir 👉 https://jsonplaceholder.typicode.com/users
*/

//👇 Creamos una función anónima autoejecutable xq en este mismo archivo Ajax aprenderemos a trabajar más adelante peticiones 
//  asíncronas con Fetch con la libreria Axios y como vamos a estar usando mismos nombres de variables y para evitar sobreescrituras
//  mejor en una función anónima autoejecutable cada ejercicio de modo q uno no afecte al otro
((d) =>{
    
    //1️⃣ INSTANCIA.- creamos una variable q haga una instancia del objeto XMLHttpRequest
    const xhr = new XMLHttpRequest(),
    
    //👇 tenemos en el html un id xhr q es una lista ordenada, esto es necesario xq x cada user q venga de la API vamos a generar un
    //  list item
    $xhr = d.getElementById('xhr'),
    
    //👇 Ahorita tenemos en la API sólo 10 users q nos va a descargar nuestra petición, pero x ejemplo los comentarios son 500, las 
    //  imagenes son 5000; entonces imaginemos estar 5 mil veces insertando un list item en el Dom, son muchas peticiones y eso va a
    //  hacer q la aplicación demande mayor rendimiento a nuestra aplicación x estar haciendo tantas inserciones al Dom, es x eso que
    //  usaremos un fragmento, este es un mecanismo especial del Dom q se crea y ya una vez se tenga todos los items se hace una sóla
    //  inserción
    $fragment = d.createDocumentFragment()
    
    /*👇 si enviamos el objeto xhr a consola, veremos algunas propiedades y eventos importantes */
    console.log(xhr)
    /* evento 'onreadystatechange' 👉 es el evento más importante
     * propiedad 'readystate' 👉 son los estados de petición: 0 , 1 , 2 , 3 o 4
     * propiedad 'response' 👉 es la respuesta q me devuelve el Servidor
     * propiedad 'responseText' 👉 es la respuesta en formato textual
     * propiedad 'status' 👉 código de respuesta del Servidor: 200 , 400 , 500
     * propiedad 'statusText' 👉 es el mensaje q me esta dando ese código de respuesta
     * propiedad 'withCredentials' 👉 nos dice si es una API pública,xq tmb tmb APIs privadas q nos pidan una clave para autenticarnos
     */
    
    //2️⃣ ASIGNACIÓN DEL EVENTO O DE LOS EVENTOS COMO ONLOAD, ONABORT, ONERROR.- el sgte paso es agregar el evento 
    //  'readystatechange', este se lanza cuando detecta cualquier cambio 
    //  de estado, ya sea q la petición se haya abortado, se haya lanzado un error, o se haya completado, x eso decimos q todos los 
    //  demás eventos q tiene Ajax están o residen en el 'readystatechange'
    xhr.addEventListener('readystatechange', e => {
        //👇 poniendo esto en consola dentro del evento 'readystatechange' nos imprime 4 Objetos XMLHttpRequest con su readyState: 4.
        //  Ahora, lo imprimió 4 veces xq esto tiene q ver con los "ESTADOS DE PETICIÓN", ya q los imprimió en consola x el estado
        //  Loading: 1, estado Loaded: 2, estado Interactive: 3 y estado Complete: 4, mas no por el estado UnInitialized xq este vale
        //  0; y como esto es dinámico, cada readyState aparece en 4 xq cuando el resultado se lanza a la consola ya ha sido 
        //  completada la petición y x eso lo esta repitiendo 4 veces
        console.log(xhr)
        
        //👇 Así q esta sería nuestra primera validación a realizar, pues haremos una condicional para q solamente cuando la propiedad
        //  readyState sea distinta a 4 no nos retorne nada, xq no nos importa si esta cargando, si ya esta cargado, o si esta en 
        //  estado interactivo, a nosotros nos importa cuando ya ha sido completado y luego de ahí ponemos console.log(xhr), xq una 
        //  vez q esta completado es q podremos hacer una manipulación en el DOM de la info q me trae el Servidor. Así q le decimos q 
        //  todo lo q no sea readyState 4 no retorne nada, esta es una validación como para q el código q esta en la callback sólo se 
        //  ejecute cuando el readystate haya sido 4
        if(xhr.readyState !== 4) return
        console.log(xhr)
        
        //  La siguiente validación obligatoria es sobre los "CÓDIGOS DE RESPUESTA", nos interesa las respuestas satisfactorias q son 
        //  los 200. Los códigos de estado de respuesta HTTP indican si se ha completado satisfactoriamente una solicitud HTTP específica.
        if(xhr.status >= 200 && xhr.status < 300){
            console.log('éxito')
            // este responseText nos trae el JSON de forma textual
            console.log(xhr.responseText)
            // Creamos una variable para parsear la respuesta JSON. JSON.parse() toma una cadena JSON y la transforma en un objeto de 
            //  JavaScript, JSON.stringify() toma un objeto de JavaScript y lo transforma en una cadena JSON.
            let json = JSON.parse(xhr.responseText)
            console.log(json)
            // como ahora nuestro JSON es un arreglo de Objetos, vamos a crear dinamicamente con un forEach etiquetas li por cada 
            //  dato de los users
            json.forEach(el => {
                const $li = d.createElement('li')
                $li.innerHTML = `${el.name} -- ${el.email} -- ${el.phone}`
                $fragment.appendChild($li)
            })
            $xhr.appendChild($fragment)
            
        }else{
            console.error('error')
            // creamos una variable para mensaje de error y como la propiedad statuText vemos q viene vacía ponemos o 'Ocurrió un 
            //  error'
            let message = xhr.statusText || 'Ocurrió un error'
            // aquí imprimimos el error en el DOM, status siempre ira xq es el código de error
            $xhr.innerHTML = `<p style='color:red;'>Error ${xhr.status}: ${message}</p>`
        }
    })
    
    //3️⃣ A ESTO TMB NECESITAMOS AGREGARLE EL EMPOYD.- la instrucción q va abrir la petición, en este caso el método open(),
    //  este método recibe 2 parámetros, el 1ro es el método x el cual vamos a comunicarnos, puede ser GET a traves de la URL,
    //  POST a través de las Cabeceras del Documento, etc. Y el 2do parámetro es la URL a la cual vamos a hacer la petición.
    xhr.open('GET','https://jsonplaceholder.typicode.com/users')     
    
    //4️⃣ FINALMENTE ENVIAMOS LA PETICIÓN
    xhr.send()
    
})(document);