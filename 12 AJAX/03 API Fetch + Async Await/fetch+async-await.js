((d) => {
    
    //! Async - Await
    //? Este keyword permite que una promesa se resuelva y retorne su valor, esto permite que podamos guardarlo en variables. Pero no todo 
    //? podía ser oro. await solo funciona en async functions. Este tipo de funciones simplemente se aseguran que lo que sea que retornen 
    //? sea una promesa.
    
    //? await hace una pausa hasta que la promesa se resuelva. Literalmente, hace que la ejecución del async function espere hasta que la 
    //? promesa se resuelva y retorne un valor, aúnque esto no detiene el engine del lenguaje, este aún puede ejecutar otros scripts o eventos 
    
    const $fetchAsync = d.getElementById('fetch-async'),
          $fragment = d.createDocumentFragment()
    
    async function getData(){
        try{
           let res = await fetch('https://jsonplaceholder.typicode.com/users'), 
           json = await res.json()
            console.log(res,json)
            
            //? throw es como un return
            //if(!res.ok) throw new Error('hssk') //? este objeto Error sólo recibe mensajes de texto, no puede recibir un objeto, x tanto
            //? haremos la sgte línea de código
            
            if(!res.ok) throw {status: res.status, statusText: res.statusText} //? esto es correcto para nuestro mensaje de error, ahora
            //? sólo falta incluirlo en el catch, vamos a eso
            
            json.forEach(el => {
                const $li = d.createElement('li')
                $li.innerHTML = `${el.name} - ${el.email} - ${el.website}`
                $fragment.prepend($li)
            })
            $fetchAsync.append($fragment)
            
        }catch(err){
            // console.error('Esto es un error',err)
            let msj = err.statusText || 'Ocurrió un error'
            $fetchAsync.innerHTML = `<p style='color:red;'>Error ${err.status}: ${msj}</p>`
        }finally{
            console.log('Esto se ejecutará independientemente del try - catch')
        }
    }

    getData()
    
})(document)