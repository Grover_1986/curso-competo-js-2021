//! Librería Axios

//? Es una librería q está basada en Promesas y nos puede ayudar mucho a la hora de hacer Peticiones Ajax
//? Repositorio de Librería Axios: https://github.com/axios/axios
//? Se puede instalar con NPM o tmb por CDN como lo haremos aquí
//? En fecth se usan dos then, pero aquí en Axios se usa un sólo then()

const d = document,
      $axios = d.getElementById('axios'),
      $fragment = d.createDocumentFragment()
    
//? Al igual q en Jquery se usa $, en Axios anteponemos la palabra axios, y tmb usamos el método get() xq estamos trabajando x la url
axios
    .get('https://jsonplaceholder.typicode.com/users')
    
    .then(res => {
        console.log(res)
        let json = res.data
        
        json.forEach((el) => {
            const $li = d.createElement('li')
            $li.innerHTML = `${el.name} -- ${el.username} -- ${el.email}`
            $fragment.prepend($li)
        })
        $axios.append($fragment)
    })
    
    //? Revisando la documentación de Axios, tenemos a response para lo q es error
    .catch(err => {
        let msj = err.response.statusText || 'Ocurrió error'
        console.error(`Error ${err.response.status}: ${msj}`)
        $axios.innerHTML = `<p style='color:red;'>Error ${err.response.status}: ${msj}</p>`
    })
    
    .finally(() => {
        console.log('Esto se ejecutará independientemente')
    }) 