const d = document,
      $axiosAsync = d.getElementById('axios-async'),
      $fragment = d.createDocumentFragment()

async function getData(){
    try{
        let res = await axios.get('https://jsonplaceholder.typicode.com/users'),
            json = await res.data
            console.log(res,json)
            
            json.forEach(el => {
                const $li = d.createElement('li')
                $li.innerHTML = `${el.name} | ${el.username} | ${el.email}`
                $fragment.prepend($li)
            })
            $axiosAsync.append($fragment)
            
    }catch(err){
        let msj = err.response.statusText || 'Ocurrió error'
        console.error(`Error ${err.response.status}: ${msj}`)
        $axiosAsync.innerHTML = `<p style='color:red;'>Error ${err.response.status}: ${msj}</p>`
        
    }finally{
        console.log('Esto se ejecutará independientemente del try - catch')
    }
}

getData()
