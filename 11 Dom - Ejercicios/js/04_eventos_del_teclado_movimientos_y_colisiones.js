const d = document
let x = 0, 
    y = 0
 
export default function moveBall(e, ball, stage){
    const $ball = d.querySelector(ball),
          $stage = d.querySelector(stage),
          limitsBall = $ball.getBoundingClientRect(),
          limitsStage = $stage.getBoundingClientRect()  //👈 El método Element.getBoundingClientRect() devuelve el tamaño de un elemento y su posición relativa respecto a la ventana de visualización (viewport).
          
          /**
           * El valor devuelto es un objeto DOMRect que es la unión de los rectángulos devueltos por getClientRects()para el elemento, es decir, las CSS border-boxes asociadas con el elemento. El resultado es el rectángulo más pequeño que contiene al elemento completo, con las propiedades de solo lectura left, top, right, bottom, x, y, width, and height describiendo la border-box total en pixels. Excepto width and height las propiedades son relativas a la esquina superior izquierda (top-left) de la ventana.
           */
          
    console.log(limitsBall, limitsStage)
        
    
    switch (e.keyCode) {
        case 37:  
            e.preventDefault() // prevenimos la acción del comportamiento por default pero 👀, el preventDefault lo tenemos q hacer en
            //cada uno de los casos xq si lo hacemos antes del switch, lo q haremos es apagar todos los comportamientos x default que 
            //tenga nuestro teclado, x eso no es una buena idea, y lo ideal para no cancelar todo el comportamiento x defecto q tenga 
            //nuestro teclado, es prevenir la acción cuando se pulsen las teclas direccionales
            if(limitsBall.left > limitsStage.left) x-- 
            break
        case 38: 
            e.preventDefault()
            if(limitsBall.top > limitsStage.top) y-- 
            break
        case 39: 
            e.preventDefault()
            if(limitsBall.right < limitsStage.right) x++
            break
        case 40: 
            e.preventDefault()
            if(limitsBall.bottom < limitsStage.bottom) y++ 
            break
        default:
            break
    }
    
    $ball.style.transform = `translate(${x * 10}px , ${y * 10}px)`  // avanza cada 10px
}