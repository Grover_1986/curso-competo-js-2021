/**
    Imaginemos q estamos en un proyecto tipo Uber, tipo Rapid y queremos q cuando el usuario entre al Sitio Web desde un dispositivo
    móvil, invitarlo o redirigirlo a una página donde esté un link para descargar la aplicación nativa y asi el usuario pueda usar 
    nuestra plataforma.
    Entonces lo q veremos hoy con el User Agent nos puede servir para redirecciones dependiendo del dispositvo, sistema operativo o la plataforma en la q el usuario nos esté visitando, o tmb para generar contenido exclusivo. 
 */

const d = document,
      n = navigator,
      ua = n.userAgent
      
export default function userDeviceInfo(id){
    const $id = d.getElementById(id),
    
    // 👇crearemos tres Objetos q nos ayudaran mucho para tomar decisiones o tmb para hacer redirecciones
    isMobile = { //👈 este objeto nos ayudará para saber si el usuario nos visita desde un sistema operativo Móvil como Android
        //👇 estas son mis válidaciones
        android: () => ua.match(/android/i),  // esta expresion regular detecta si en la cadena del User alguien tine Android 
        ios: () => ua.match(/iphone|ipad|ipod/i), // o si tiene la palabra iphone, ipad o ipod
        windows: () => ua.match(/windows phone/i),
        any: function(){ //👇esta funcion anónima hace referencia a los otros tres
            return this.android() || this.ios() || this.windows()
        }
    },  
    
    isDesktop = { //👈 este objeto nos permite detectar si el usuario nos visita desde un sistema operativo para Desktop
        linux: () => ua.match(/linux/i),
        mac: () => ua.match(/mac os/i),
        windows: () => ua.match(/windows nt/i),
        any: function(){
            return this.linux() || this.mac() || this.windows()
        }
    },  
    
    isBrowser = { //👈 este objeto nos ayuda a detectar en que Navaegador estamos.
        chrome: () => ua.match(/chrome/i),
        safari: () => ua.match(/safari/i),
        firefox: () => ua.match(/firefox/i),
        opera: () => ua.match(/opera|opera mini/i),
        ie: () => ua.match(/msie|iemobile/i),
        edge: () => ua.match(/edge/i),
        any: function(){
            return(
                this.chrome() ||
                this.safari() ||
                this.firefox() ||
                this.opera() ||
                this.ie() ||
                this.edge() ||
                this.any()
            )
        }
    }  
    
    $id.innerHTML = `
        <ul>
            <li><b>User Agent: </b><i>${ua}</i></li>
            <li><b>Plataforma: </b><i>${isMobile.any() ? isMobile.any() : isDesktop.any()}</i></li>
            <li><b>Navegador: </b><i>${isBrowser.any()}</i></li>
        </ul>
    `
    
    /* Contenido exclusivo */
    if(isBrowser.chrome()){
        $id.innerHTML += `<p><mark>Este contenido sólo se ver en Chrome</mark></p>`
    }
    
    if(isBrowser.firefox()){
        $id.innerHTML += `<p><mark>Este contenido sólo se ve en Firefox</mark></p>`
    }
    
    if(isDesktop.linux()){
        $id.innerHTML += `<p><mark>Descarga nuestro software para Linux</mark></p>`
    }
    
    if(isDesktop.mac()){
        $id.innerHTML += `<p><mark>Descarga nuestro software para Mac OS</mark></p>`
    }
    
    if(isDesktop.windows()){
        $id.innerHTML += `<p><mark>Descarga nuestro software para Windows</mark></p>`
    }
    
    /* Redirecciones */
    if(isMobile.android()){ //👈 cuando nuestro sitio web esté en mobile y sea Android nos redirigira a la página de crisbalTI
        window.location.href = 'https://crisbalti.com' 
    }
    
    console.log(isMobile.android())
    console.log(isMobile.ios())
    console.log(isMobile.any()) //👈 este método any nos sirve para válidar q mientras el usuario esté en un dispositivo móvil, lo demás como el sistema operativo, dde q compania: eso no importaba, con tal de q sólo sea un dispositivo móvil.
}