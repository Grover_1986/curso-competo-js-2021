const d = document

// esta función necesitará el selector del Boton y el Selector en el q vamos analizar el código Html y de ahi vamos a obtener el ganador
export default function draw(btn, selector){
    //👇 creamos una función expresada
    const getWinner = (selector) => {
        // 👇 traemos en una constante todos los elementos q coinciden con selector
        const $players = d.querySelectorAll(selector),
        // 👇creamos una variable q nos permita saber un número aleatorio, como random nos da un valor entre 0 y 1, entonces lo multplicamos por la longitud del nro de participantes, y con floor lo redondeamos hacía abajo
        random = Math.floor(Math.random() * $players.length),
        // 👇y aquí traemos al ganador en la posición que nos da random
        winner = $players[random]
        console.log($players, random, winner)
        //👇retornamos al ganador y como winner es un list item extraemos el contenido con textContent
        return `El ganador es: ${winner.textContent}`
    }
    // 👇hacemos una delegación de eventos
    d.addEventListener('click', e => {
        //👇 Sí el objeto q dispara el evento encuentra al selector btn
        if(e.target.matches(btn)){
            // 👇guardamos al ganador en la variable result
            let result = getWinner(selector)
            alert(result)
            console.log(result)
        }
    })
}


// Con esta función tmb podemos hacer sorteos en una zona de comentarios de Youtube, Facebook o cualquier red social, sólo abrimos la consola y ejecutamos
//👇 la función para q quede guardada en memoria, luego tendremos q buscar el selector padre de cada hilo de comentarios y escarbar hasta llegar al nombre de usuario, esto lo podemos realizar en facebook, twitter, etc, para hacer nuestro propio sorteo y sin recurrir a programas de terceros.
const getWinnerComment = (selector) => {
    const $players = document.querySelectorAll(selector),
    random = Math.floor(Math.random() * $players.length),
    winner = $players[random]
    return `El ganador es: ${winner.textContent}`
}
//👇tendremos q buscar el selector padre de cada hilo de comentarios y escarbar hasta llegar al nombre de usuario
// getWinnerComment('ytd-comment-renderer #author-text span')