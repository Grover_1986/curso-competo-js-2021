/* ********** Menú Hamburguesa ********** */
export default function hamburgerMenu (btn, panel, menuLink){
    const d = document
    d.addEventListener('click', (e) => {
        if(e.target.matches(btn) || e.target.matches(`${btn} *`)){
            d.querySelector(btn).classList.toggle('is-active')
            d.querySelector(panel).classList.toggle('is-open')
        }
        
        if(e.target.matches(menuLink)){
            d.querySelector(panel).classList.toggle('is-open')
            d.querySelector(btn).classList.toggle('is-active')
        }
    })
}
