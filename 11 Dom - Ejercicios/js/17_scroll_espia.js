const d = document
//👇 en esta función no necesitaremos ningún parámetro xq si esta función se quiere reutilizar, tendremos q respetar y usar las etiquetas html con sus atributos y el css tal como estan. 
export default function scrollSpy(){
    //1️⃣ debemos saber q cada sección del contenido esta asociada a un elemento del menu
    //2️⃣ el API IntersectionObserver necesita saber q en el foco del ViewPort haya  una sección visible y tmb necesitara saber con que enlace esta relacionado, y es ahi donde se hace el match entre los enlaces y nuestras secciones del sitio one page.
    //3️⃣ en ese caso a todos los enlaces le agregaremos un data-attribute y tmb a las secciones, ese mismo data-attribute
    
    //👇 el observer necesitarà estos elementos para observarlos, es decir a las secciones q iremos scrolleando
    const $sections = d.querySelectorAll('section[data-scroll-spy]')
    
    //👇aqui creamos la callback, y las entries son elementos q estan entrando a la visualizaciòn del viewPort
    const cb = (entries) => {
        
        console.log('Entradas', entries)
        
        //👇aqui x cada entrada (section) q tengamos ejecutamos un console.log
        entries.forEach(entry => {
            console.log('entry', entry)//👈si nos damos cuenta, esto nos da en la consola una propiedad boolean isIntersecting q significa sì un elemento ya esta en ese limite donde se empieza a ver el elemento conforme scrolleamos
            
            //👇sabiendo q es isIntersecting, ahora hacemos una condicion para q en base a esta propiedad, apliquemos al enlace q tenemos en el menu, la clase active
            const id = entry.target.getAttribute('id')
            // console.log(id)
            if(entry.isIntersecting){
                d.querySelector(`a[data-scroll-spy][href="#${id}"]`).classList.add('active')
            }else{
                d.querySelector(`a[data-scroll-spy][href="#${id}"]`).classList.remove('active')
            }
            //☝ Vemos q con ese còdigo nos remarca de dos en dos en el menu, eso es xq el viewport visualiza dos secciones, para arreglar eso y q nos capture de uno en uno, manejaremos las opciones q nos da el new IntersectionObserver(cb, {})
        }) 
    }
    
    //👇 creamos un Oservador e instanciamos el obj IntersectionObserver() q usa como paràmetros una callback y una serie de opciones
    const observer = new IntersectionObserver(cb, {
        //root 👈 esta propiedad es la raiz principal q serìa el documento
        //rootMargin: '-250px'  //👈esta propiedad nos sirve para dar margen, aqui le damos -250px es como si visualizaramos toda la parte del IntersectionObserver hacìa dentro
        threshold: [0.5 , 0.75] //👈 esta propiedad es muy interesante, trabaja con lìmites, aqui le estamos dando el 50%, trabaja con procentajes y sì se le pone [] se maneja entre rangos de %
    })
    //👇si deseamos ver lo q hay en observer
    console.log('observador', observer)
    
    //👇 y aqui por cada secciòn q tenga mi documento con ese atributo data-scroll-spy, le asiganmos el observador y en su mètodo observe() recibe el target al q queremos asignarle ese observador 
    $sections.forEach(el => observer.observe(el))
}