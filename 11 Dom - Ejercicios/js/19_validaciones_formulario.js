const d = document

export default function contactFormValidations(){
    const $form = d.querySelector('.contact-form'),
        $inputs = d.querySelectorAll('.contact-form [required]')
   
    $inputs.forEach(input => {
        const $span = d.createElement('span')
        $span.id = input.name
        $span.textContent = input.title //👈 Al span le ponemos lo q hay en el atributo title del input
        $span.classList.add('contact-form-error','none')
        input.insertAdjacentElement('afterend',$span)
    })
    //1️⃣ Ahora, la pregunta es ¿Dónde vamos hacer las validaciones?, pués podriamos poner las validaciones en el momento q se envía el formulario (evento submit).
    //2️⃣ Pero nosotros lo haremos un poco más interactivo, lo haremos en el evento 'keyup', para q al momento de q el usuario vaya introduciendo, la validación se vaya cumpliendo.
    d.addEventListener('keyup', e => {
        if(e.target.matches('.contact-form [required]')){
            let $input = e.target,
                //👇 aqui decimos q sí el input no tiene un atributo pattern, entonces seguramente tiene un data-attribute, y para acceder a un data-attribute, escribimos dataset
                patron = $input.pattern || $input.dataset.pattern
                // console.log($input, patron)
                
            if(patron && $input.value !== ''){
                // console.log('El input tiene patron')
                //👇 Aquí cuando tengan patron creamos una variable para guardar nuestra Expresión Regular
                let regex = new RegExp(patron)
                //👇 aqui decimos sí el valor del input no cumple con la Expresión Regular
                return !regex.exec($input.value)
                    ? d.getElementById($input.name).classList.add('is-active') //👈 entonces capturamos el id del span q creamos dinamicamente y le estamos agregando la clase de la alerta de error
                    : d.getElementById($input.name).classList.remove('is-active') //👈 caso contrari si todo esta bien. quitamos la clase de alerta
            }
            if(!patron){
                // console.log('El input NO tiene patron')
                return $input.value === ''
                    ?  d.getElementById($input.name).classList.add('is-active')
                    :  d.getElementById($input.name).classList.remove('is-active')
            }
        }
    })
    
    //📄 NOTA: sí no sabemos programar del lado del Servidor, podemos usar una herramienta online para tener un formulario funcional en nuestros sitios
    // link de la herramienta 👉 https://formsubmit.co/, nuestros inputs deben tener name,y sólo tenemos q agregar el action, el method y editar el correo
    // pero esta herramienta funciona desde un entorno de servidor aunque sea local, pero q sea http, sí lo abrimos desde nuestro local con file en unidad C, no funcionará: y tmb no nos olvidemos de quitar el preventDefault()
    //👇 Ahora trabajaremos con el evento SUBMIT para enviar los datos del formulario
    d.addEventListener('submit', e => {
        // e.preventDefault()
        alert('Enviando formulario')
        // Ahora bien, si vamos hacer una petición a una API, a un Servidor o a un PHP, o a otra tecnología del lador del Servidor, lo ideal para
        // q el usuario no se desespere, es ponerle un loader; así q trabajaremos con nuestro loader q está oculto,
        // pero primero crearemos dos variables, una q almacene la referencia en el DOM del Loader y otra q alamacene la ref. de la repuesta
        const $loader = d.querySelector('.contact-form-loader'),
            $response  = d.querySelector('.contact-form-response')
            
        //👇 le quitamos la clase none al loader para q se pueda ver
        $loader.classList.remove('none')
        
        setTimeout(() => {
            $loader.classList.add('none')
            $response.classList.remove('none')
            $form.reset()
            
            setTimeout(() => $response.classList.add('none'), 3000)
        },2000)
    })
}