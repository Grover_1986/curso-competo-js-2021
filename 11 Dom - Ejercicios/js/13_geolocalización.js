const d = document,
    n = navigator
    
export default function getGeolocation(id){
    const $id = d.getElementById(id),
    //👇adicionalmente la funcion necesitara estas opciones
    options = {
        enableHighAccuracy: true, //👈 con esto le estamos diciendo q el dispositivo haga la mejor lectura posible q pueda
        timeout: 5000, //👈tiempo de espera para tomar la lectura
        maximumAge: 0//👈es para evitar q no se guarden en Caché las lecturas
    }
    //👇aqui definimos nuestra función en caso de éxito, esta funcion nos devolverá la posición, q es un objeto.
    const success = (position) => {
        let coords = position.coords
        console.log(position)
        $id.innerHTML = `
            <p>Tu posición actual es:</p>
            <ul>
                <li>Latitud: <b>${coords.latitude}</b></li>
                <li>Longitud: <b>${coords.longitude}</b></li>
                <li>Precisiòn: <b>${coords.accuracy}</b> metros</li>
            </ul>
            <a href='https://www.google.com/maps/@${coords.latitude},${coords.longitude},20z' target='_blank'>Ver en Google Maps</a>
        `
    }
    // 👇esta función es en caso haya un error
    const error = (err) => {
        $id.innerHTML = `<p><mark>Error N° ${err.code}: ${err.message}</mark></p>`
        console.log(`Error N° ${err.code}: ${err.message}`)
    }
    //👇 para obtener la ubicación actual tenemos getCurrentPosition
    n.geolocation.getCurrentPosition(success, error, options) //👈getCurrentPosition() necesita una funcion q se ejecute en caso de éxito, otra función en caso de error y las opciones
}

/**
 * 📄NOTA:
 * Rastreando la posición actual 👉 watchPosition()
 * Si los datos de ubicación cambian (si el dispositivo se mueve o información geográfica más precisa es recibida), puede definir una función de 
 * callback que se ejecuta al cambiar la posición. Esto se logra a través de la función watchPosition(), que recibe los mismos parámetros que 
 * getCurrentPosition(). La función de callback es ejecutada varias veces, permitiendo al navegador actualizar la ubicación cada vez que cambia, o 
 * proporcionar una posición con mayor exactitud utilizando distintas técnicas de geolocalización. La función de callback de error, la cual es 
 * opcional como en getCurrentPosition(), es llamada solo una vez, cuando nunca serán devueltos resultados correctos.
 */