import hamburgerMenu from './01_menu_hamburguesa.js'
import {relojDigital, alarmaSonora} from './02_reloj_digital_y_alarma.js'
import shortcuts from './03_atajo_teclado_shortcuts.js'
import moveBall from './04_eventos_del_teclado_movimientos_y_colisiones.js'
import countdown from './05_cuenta_regresiva.js'
import scrollTopButton from './06_scroll_top_boton.js'
import darkTheme from './07_tema_oscuro.js'
import responsiveMedia from './08_objeto_responsive.js'
import responsiveTester from './09_responsive_tester.js'
import userDeviceInfo from './10_deteccion_dispositivos.js'
import networkStatus from './11_deteccion_red.js'
import webCam from './12_deteccion_webcam.js'
import getGeolocation from './13_geolocalización.js'
import searchFilter from './14_filtro_busqueda.js'
import draw from './15_sorteo.js'
import slider from './16_carrusel.js'
import scrollSpy from './17_scroll_espia.js'
import smartVideo from './18_video_inteligente.js'
import contactFormValidations from './19_validaciones_formulario.js'
import speechReader from './20_narrador.js'

const d = document

d.addEventListener('DOMContentLoaded', (e) => {
    hamburgerMenu('.hamburger','.panel','.menu-link')
    relojDigital('#iniciar-reloj','#detener-reloj','.reloj-alarma','div')
    alarmaSonora('#iniciar-alarma','#detener-alarma','alarma-morning-mix')
    countdown('#countdown','2021/12/25','¡Feliz Navidad! 🎁🎂🥧')
    scrollTopButton('.scroll-top-btn')
    responsiveMedia(
        'youtube',
        '(min-width: 1024px)',
        '<a href="https://youtu.be/6kUZ_O42lkQ" target="_blank">Ver Vídeo</a>',
        '<iframe width="560" height="315" src="https://www.youtube.com/embed/6kUZ_O42lkQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>')
    responsiveMedia(
        'gmaps',
        '(min-width: 1024px)',
        '<a href="https://goo.gl/maps/7op92QQcEWd9Uz4u9" target="_blank">Ver Mapa</a>',
        '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3901.9639915950193!2d-77.03273448494129!3d-12.045998545166611!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c8b5d5aa7eb1%3A0x16061e0b481e22aa!2sPlaza%20de%20Armas%20de%20Lima!5e0!3m2!1ses!2spe!4v1622139722224!5m2!1ses!2spe" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>')
    responsiveTester('responsive-tester')
    userDeviceInfo('user-device')
    webCam('webcam')
    getGeolocation('geolocation')
    searchFilter('.card-filter','.card')
    draw('#winner-btn','.player')
    slider()
    scrollSpy()
    smartVideo()
    contactFormValidations()
})

d.addEventListener('keydown', (e) => {
    shortcuts(e)
    moveBall(e,'.ball','.stage')
})

d.addEventListener('keyup', (e) => {
    
})

darkTheme('.dark-theme-btn','dark-mode')
networkStatus()
speechReader()