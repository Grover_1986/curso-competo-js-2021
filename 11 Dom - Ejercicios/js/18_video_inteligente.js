const d = document,
      w = window
      
export default function smartVideo(){
    // 👇capturamos todos los videos q tengan el atributo [data-smart-video]
    const $videos = d.querySelectorAll('video[data-smart-video]')
    // 👇aquí creamos la función callback, entries sería como el event de addEventListener
    const cb = (entries) => {
        // 👇aquí decimos q x cada una de la entradas q va a observar el IntersectionObserver, ejecutemos la sgte programación
        entries.forEach(entry => {
            // 👇sí ya se ha dado la Intersección
            if(entry.isIntersecting){
                entry.target.play()
            }else{
                entry.target.pause()
            }
            // El objeto Document tiene una propiedad llamada visibilityState, y será evaluada en el evento visibilitychange del objeto Window.
            //👇 Esto será para q al cambiar de pestaña, el video se pause, y cuando volvamos se reproduzca automaticamente.
            w.addEventListener('visibilitychange', e => {
                (d.visibilityState === 'visible') ?  entry.target.play() :  entry.target.pause()
            })
        })
    }
    // 👇creamos un nuevo objeto de la API IntersectionObserver
    const observer = new IntersectionObserver(cb,{threshold: .5}) //👈IntersectionObserver() usa como parámetro una callback q sería como una función, y tmb opciones
    
    // 👇aquí decimos aq elementos del DOM le vamos asignar dicho Observador
    $videos.forEach(el => observer.observe(el))
}