const d = document,
      w = window,
      n = navigator
      
export default function networkStatus(){
    const isOnline = () => {
        const $div = d.createElement('div')
        
        if(n.onLine){
            $div.textContent = 'Conexión Reestablecida'
            $div.classList.add('online')
            $div.classList.remove('offline')
        }else{
            $div.textContent = 'Conexión Perdida'
            $div.classList.add('offline')
            $div.classList.remove('online')
        }
        
        d.querySelector('#seccion7').append($div)
        setTimeout(() => d.querySelector('#seccion7').removeChild($div),2000)
    }
    
    //👇 recurriremos a dos eventos de la ventana en q se va a ejecutar nuestra detección de la conexión
    w.addEventListener('online', (e) => isOnline()) //👈 ejecutamos la funcion en los eventos online y offline
    w.addEventListener('offline', (e) => isOnline()) //👈 Navigator tiene la propiedad n.onLine, ésta nos detecta si estamos conectados o no a Internet
}

/*📄NOTA:
En la consola, en menu Application, damos click en Service Workers y vemos q hay un botoncito Offline, ese botoncito nos permite simular 
la desconexión a internet */