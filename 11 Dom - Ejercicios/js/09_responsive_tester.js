
const d = document

/* ********** Responsive Tester ********** */
export default function responsiveTester(form){
    const $frm = d.getElementById(form)
    let tester;
              
    d.addEventListener('submit', e => {
        e.preventDefault()
        if(e.target === $frm){
            /**
             * open utiliza 3 parámetros:
             * 1️⃣ la URL
             * 2️⃣ el nombre de la ventana
             * 3️⃣ caracteristicas como tamaño, posición, barras de desplazamiento, etc
             */ 
            tester = window.open($frm.direccion.value,'Tester', `innerWidth=${$frm.ancho.value} innerHeight=${$frm.alto.value}`)
        }
    })    
    
    d.addEventListener('click', e => {
        if(e.target === $frm.cerrar) tester.close()
    })
}

/**
 * 📄NOTA:
 *1️⃣Cuando un formulario no tiene action, el formulario autoprocesa la página en la q se encuentra, para evitar ese proceso se le   
     pone e.preventDefault(), y ya no se procesa la página.
 *2️⃣Una de las ventajas de usar un formulario es q podemos acceder con la anotacion del punto, a cualquier input q esté dentro del 
     formulario, llamandolo con el atributo name.
 */