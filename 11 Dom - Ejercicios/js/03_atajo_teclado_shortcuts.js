/**
 * Aquí realmenete no tenemos q hacer Delegación de Eventos, xq la detección de los eventos del teclado debe asignarse al modo raíz, en este
 * caso el Document.
 */

/* ********** Atajos de teclado ********** */
export default function shortcuts(e){   
    if(  e.key === 'a' && e.altKey){
        alert(`Holaaa Alt + ${e.key}`)
    }
    if(e.altKey && e.key === 'p'){
        prompt('',`Ctrl + ${e.key}`)
    }
    if( e.key === 'c' && e.altKey){
        confirm(`Ctrl + ${e.key}`)
    }
}