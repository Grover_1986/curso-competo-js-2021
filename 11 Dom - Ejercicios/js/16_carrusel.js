const d = document

export default function slider(){
    const $btnPrev = d.querySelector('.prev'),  //👈 capturamos el boton de atrás
        $btnNext = d.querySelector('.next'), //👈 capturamos el boton de siguiente
        $slides = d.querySelectorAll('.slider-slide') //👈 capturamos todos los slides
    
    let i = 0 //👈 creamos una variable contador q cuando haya llegado al último slide pueda regresarse a la primera y viceversa
    d.addEventListener('click', e => {  //👈 hacemos la delegación de eventos
        
        if(e.target === $btnPrev){ //👈 aqui no usamos matches, sino q comparamos el objeto q origino el evento con la variable q tiene guardada la clase
            e.preventDefault() //👈cancelamos el atributo href para q no se nos vaya hasta arriba cuando hagamos click
            $slides[i].classList.remove('active')   //👈 quitamos la clase active del 1er slide
            i-- //👈no puede haber el -1 asi q haremos una condicional
            if(i < 0){  //👈 en esta condicional le decimos q sí i es menor q 0, i vale la longitud q es 4 slides, pero le restamos -1
                i = $slides.length -1   
            }
            $slides[i].classList.add('active') // y aqui le agregamos la clase active para q funcione
        }
        
        if(e.target === $btnNext){
            e.preventDefault()
            $slides[i].classList.remove('active')
            i++
            if(i >= $slides.length){
                i = 0
            }
            $slides[i].classList.add('active')
        }
    })
}