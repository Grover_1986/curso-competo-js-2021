/**
 * Generalmente para cambiar de color de tema podemos trabajar aplicandolo al Body o a la etiqueta Html, pero imaginemos q necesitamos
    aplicar esa clase a varios elementos del Html, pues para eso vamos ayudarnos de los data Attribute.
 * En esta función trabajaremos con los data Attribute, y el parámetro dataDark nos servira para eso.
 */

const d = document,
      ls = localStorage

/* ********** Tema Dark/Light ********** */   
export default function darkTheme(btn, classDark){
    const $themeBtn = d.querySelector(btn),
          $selectors = d.querySelectorAll('[data-dark]'),
          $selectorsYellow = d.querySelector('[data-yellow]'),
          $selectorsDark = d.querySelector('[data-dark1]')
          console.log($selectors)
          
    //👇 guardamos los emojis en variables xq cuando presionemos el boton, aparte de agregar o quitar la clase a los elementos q tengan
    //   el atributo data-dark, tmb tienen q cambiar su textContent o por la luna o por el sol
    let moon = '🌙', 
        sun = '🌞'
        
    //👇 estas funciones van a tener las líneas de código q hacen cambiar la luna x el sol y q aplican o quitan la clase de CSS a los 
    //  elementos q se les aplica el darktheme, se tienen q ejecutar tanto al click del boton como al DOMContentLoaded a la hora q recarguemos
    const lightMode = () => {
        $selectors.forEach(el => el.classList.remove(classDark))
        $selectorsYellow.classList.remove('yellow')
        $selectorsDark.classList.remove('dark')
        $themeBtn.textContent = moon
        ls.setItem('theme','light') //👈 las funciones de lightMode y darkMode tmb nos sirven para almacenar el último cambio en el localStorage, asi q nos aseguramos y establecemos sus respectivos valores
    }
    const darkMode = () => {
        $selectors.forEach(el => el.classList.add(classDark))
        $selectorsYellow.classList.add('yellow')
        $selectorsDark.classList.add('dark')
        $themeBtn.textContent = sun
        ls.setItem('theme','dark')
    }
    
        d.addEventListener('click', e => {
            if(e.target.matches(btn)){
               if($themeBtn.textContent === moon){
                  darkMode()
               }else{
                 lightMode()
               }
            }
        })
        
        d.addEventListener('DOMContentLoaded', e => {
            // Ahora aqui, cuando cargue el Documento tenemos q preguntar si existe una variable de tipo localStorage, q es lo q nos va a estar controlando el darkTheme
            console.log(ls.getItem('theme'))
            
            //👇 si no existe una variable de tipo localStorage, la creamos
            if(ls.getItem('theme') === null) ls.setItem('theme','light') //👈 setItem() usa 2 parámetros, 1. el nombre dela variable y 2. el valor
         
            if(ls.getItem('theme') === 'light') lightMode() //👈 si el valor de la variable theme de tipo localStorage es igual a light, // ejecutamos el código para poner el tema claro, y eso esta dentro de la funcion lightMode()
            
            if(ls.getItem('theme') === 'dark') darkMode()
        })
    
}

/**
 *  📄NOTA:
 *  El LocalStorage es una API que nos permite almacenar información en el Navegador, en el disco duro del usuario.
 *  Hay dos almacenamientos, local y webSorage q es como una sesion de php pero de lado del Navegador.
 *  Em localStorage se puede guardar hasta 5 megas de información 
 * 
 *   getItem().- obtiene valores
 *   setItem().- establece valores
 */