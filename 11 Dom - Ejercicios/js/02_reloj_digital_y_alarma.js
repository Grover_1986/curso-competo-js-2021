
const d = document

/* ********** Reloj Digital ********** */   
function relojDigital(btnInicio, btnDetener, contenedor, contenido){
  const $div = d.createElement(contenido),
    
    reloj = () => {
        const $hora = new Date().toLocaleTimeString()
        $div.classList.add('hora')
        $div.textContent = `${$hora}`
        d.querySelector(contenedor).prepend($div)
    }
    
    let interval;
    d.addEventListener('click', (e) => {
        if(e.target.matches(btnInicio)){
           interval = setInterval(() => {
               reloj()
            }, 1000)
            d.querySelector(btnInicio).style.setProperty('display','none')
            $div.style.setProperty('display','block')
        }
        if(e.target.matches(btnDetener)){
            clearInterval(interval)
            $div.style.setProperty('display','none')
            d.querySelector(btnInicio).style.setProperty('display','inline-block')
        }
    })
}

 
/* ********** Alarma Sonora ********** */
function alarmaSonora(btnIniciar, btnDetener, nombreAlarma){
    const audio = new Audio(`audio/${nombreAlarma}.mp3`)
    let alarmaTiempoEspera;
       
    d.addEventListener('click', (e) => {
        if(e.target.matches(btnIniciar)){
            alarmaTiempoEspera = setTimeout(() => {
                audio.play()
            },2000)
            e.target.disabled = true    
        }
        if(e.target.matches(btnDetener)){
            clearTimeout(alarmaTiempoEspera)
            audio.pause()
            audio.currentTime = 0 //👈 con esto reiniciamos el sonido
            d.querySelector(btnIniciar).disabled = false
        }
    })
}

export  {relojDigital, alarmaSonora}