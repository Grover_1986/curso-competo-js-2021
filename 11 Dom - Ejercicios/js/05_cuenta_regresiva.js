const d = document

// Para trabajar operaciones con fechas es necesario convertirlas en milisegundos
export default function countdown(id, fechaFutura, mensajeFinal){
    const $countdown = d.querySelector(id),
          countdownDate = new Date(fechaFutura).getTime() //👈 obtenemos el valor de la fecha futura en milisegundos
          
    let countdownTiempo = setInterval(() => {
        let now = new Date().getTime()  //👈 instante de fecha actual en milisegundos
        let restoFecha, diasMs, horasMs, minutosMs, segundosMs;
        restoFecha = countdownDate - now
        diasMs = 1000 * 60 * 60 * 24 
        horasMs = 1000 * 60 * 60
        minutosMs = 1000 * 60
        segundosMs = 1000
        
        let dias, horas, minutos, segundos;
        segundos = (Math.floor((restoFecha / segundosMs) % 60) < 10) ? `0${Math.floor((restoFecha / segundosMs) % 60)}` 
                    : Math.floor((restoFecha / segundosMs) % 60)
                    
        minutos = (Math.floor((restoFecha / minutosMs) % 60) < 10) ? `0${Math.floor((restoFecha / minutosMs) % 60)}` 
                : Math.floor((restoFecha / minutosMs) % 60)
                
        horas = (Math.floor((restoFecha / horasMs) % 24) < 10) ? `0${Math.floor((restoFecha / horasMs) % 24)}` 
                : Math.floor((restoFecha / horasMs) % 24) 
                
        dias = Math.floor((restoFecha / diasMs) % 365)
            
        $countdown.innerHTML = `<p><span>Faltan:</span><span>${dias} días</span><span>${horas} horas</span><span>${minutos} minutos</span><span>${segundos} segundos</span></p>`
        
        if(restoFecha <= 0){
            clearInterval(countdownTiempo)
            $countdown.innerHTML = `<h3 class=saludo>${mensajeFinal}</h3>`
        }
    }, 1000);
    
}
