const d = document,
      w = window
/**
 * Para este ejercicio de reponsive con JavaScript usaremos unos enlaces, un video youtube y un mapa de google, pero tmn podriamos usar
 * cualquier contenido q no sea texto, como un video, un audio de html o embeber una linea de tiempo de comentarios de twitter, o una 
 * linea de comentarios de facebook, o una foto de instagram 
 */

/**
 * Para esta función necesitaremos 4 parámetros:
 *1️⃣ id.- el elemento id al q va a estar revisando
 *2️⃣ mq.- la media querie q nos servira para pasar justamente del enlace dado, al documento ya embebido
 *3️⃣ mobileContent.- contenido Html q queremos ver a nivel mobile
 *4️⃣ desktopContent.- contenido q queremos q se cargue dinamicamente en el desktop
 */
export default function responsiveMedia(id, mq, mobileContent, desktopContent){
    //👇 variable q guardara la media querie que nos pase el parámetro mq
    let breakpoint = w.matchMedia(mq)
    
    const responsive = (e) => {
        if(e.matches){  //👈esto nos dará un valor booleano, true o false cuando la media querie se cumpla
            d.getElementById(id).innerHTML = desktopContent
        }else{
            d.getElementById(id).innerHTML = mobileContent
        }
        console.log('MQ',breakpoint,e.matches)
    }
    
    //👇 Podemos agregarle un listener al matchMedia, pero no un addEventListener
    breakpoint.addListener(responsive) //👈 le pasamos una función
    responsive(breakpoint) // con esto hemos resuelto el ejercicio con el método DOMContentLoaded o window.load
}

/**
 * 📄NOTA:
 * eL addListener se ejecuta cuando hay un cambio, en este caso la media querie
 */