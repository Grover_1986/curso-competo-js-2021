const d = document,
      n = navigator
      
/**
 * El método MediaDevices.getUserMedia() solicita al usuario permisos para usar un dispositivo de entrada de vídeo y/o uno de audio como una cámara o
 * compartir la pantalla y/o micrófono. Si el usuario proporciona los permisos, entonces le retornará un Promise (en-US) que es resuelto por el resultado
 * del objeto MediaStream. Si el usuario niega el permiso, o si el recurso multimedia no es válido, entonces el promise es rechazado con
 * PermissionDeniedError o NotFoundError respectivamente. Nótese que es posible que el promise retornado no sea ni resuelto ni rechazado, ya que no se
 * requiere que el usuario tome una decisión.
 */
      
export default function webCam(id){
    const $video = d.getElementById(id)
    
    //👇 hacemos una validación para ver si existe la función en el Navegador q estamos consultando
    if(n.mediaDevices.getUserMedia){
        /* esta API es una Promesa y podemos ejecutar el método then() para saber q hacer con el stream de datos q nos va a devolver la función 
         getUserMedia(), y tmb si hay algún error lo podremos capturar en el método catch()*/
        n.mediaDevices
        .getUserMedia({video: true, audio: true}) //👈getUserMedia() recibe una serie de parámetros
        .then(stream => { //👈podemos ejecutar el método then() para saber q hacer con el stream de datos q nos va a devolver la función getUserMedia()
            console.log(stream) //👈 stream es un Objeto
            $video.srcObject = stream /*👈source esta esperando recibir una URL (cadena de texto), pero como stream es un Objeto, en lugar de acceder al
             source del video, vamos acceder a una propiedad llamada srcObject, es decir mandarle la misma fuente pero a manera de Objeto*/
            $video.play()
        })
        .catch((err) => {
            $video.insertAdjacentHTML('beforebegin',`<p><mark>Sucedió el siguiente error: ${err}</mark></p>`)
            console.log(`Sucedió el siguiente error: ${err}`)//👈si hay algún error lo podremos capturar en el método catch()
        })  
    }
}