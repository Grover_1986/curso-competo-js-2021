
const d = document,
      w = window

// utilizaremos los objetos Document y Window
export default function scrollTopButton(btn){
    const $btnScroll = d.querySelector(btn)
    
    w.addEventListener('scroll', (e) => {
        if(w.scrollY >= 1500) $btnScroll.classList.remove('hidden')
        else $btnScroll.classList.add('hidden')
    })
    
    d.addEventListener('click', (e) => {
        if(e.target.matches(btn)){
            w.scrollTo({  // scrollTo es un Objeto q recibe tres opciones: 1) el comprtamiento, 2) hacia donde deseamos q regrese la barra de scroll 
                behavior: 'smooth',
                top: 0
            })
        }
    })
    
}
 