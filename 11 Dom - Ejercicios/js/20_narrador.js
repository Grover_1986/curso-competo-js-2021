const d = document,
      w = window

// 👉 no usaremos ningún selector como parámetro, xq este ejercicio será experimental e internamente llamaremos a nuestros selectores
// 👀 OJO: en esta función usaremos el DOMContentLoaded internamente, es x eso q cuando llamemos a esta función en el archivo dom_ejercicios.js, 
//   lo llamaremos solito, fuera de ese DOMContentLoaded

export default function speechReader(){
    
    const $speechSelect = d.querySelector('#speech-select'),
          $speechTextarea = d.querySelector('#speech-text'),
          $speechBtn = d.querySelector('#speech-btn'),
          //👇ahora creamos una variable q no es parte del DOM, q es la q guardara el API q nos permite leer esos mensajes 
          $speechgMessage = new SpeechSynthesisUtterance() //👈esto nos permite interactuar con las voces del Sistema Operativo
          
        //👇ahora mandamos a consola nuestra variable q nos va a permitir interactuar con las voces y veremos q es un Objeto q tiene
        // ciertos eventos y algunas propiedades como pitch, rate y veamos q tiene una propiedad llamada voice y está en null, está
        // así xq a esta propiedad se le puede asignar una voz en particular
          console.log($speechgMessage)
    
    //📄NOTA: Las voces las vamos a invocar a la hora q carguemos el documento, entonces de inicio las voces veremos q son una
    //  especie de NodeList q viene en una especie de arreglo, entonces vamos a crear nuestra variable voices q de inicio será un
    // 👇 arreglo vacío, pero al momento q solicitemos todas las voces a la q tiene acceso el Navegador a traves del Sistema
    //    Operativo, se van a ir llenando en nuestro arreglo de voces
    let voices = []
    
    //📄IMPORTANTE: Las voces se encuentran en una API que cuelga del window, x eso ni siquiera es necesario especificar window, ésta
    //  API se llama SpeechSynthesis; si ejecutamos este objeto en consola veremos q nos da algunas propiedades pero tmb nos da un 
    //  evento llamado 'onvoiceschanged', éste sirve para cambiar de una voz a otra
          
    //📄IMPORTANTE: El objeto SpeechSynthesis tiene un método q se llama getVoices(), éste nos devolverá unas 22 diferentes voces,
    // estas voces pueden variar dependiendo del Navegador y del Sistema Operativo, cada una de estas voces es un objeto
    
    //👇 Ahora, necesitaremos 3 eventos q vamos a delegar al document
    
    d.addEventListener('DOMContentLoaded', e => { //👈como necesitamos el DOMContentLoaded aquí, ya no necesitamos incluir nuestra
      // función speechReader() dentro del DOMContentLoaded de nuestro archivo dom_ejercicios.js
      
      //👇 al poner esto en consola nos devuelve un arreglo vacío, es decir no nos carga las voces inmediatamente, 
       console.log(w.speechSynthesis.getVoices())
       
      //👇 Vemos q al poner speechSynthesis en consola nos da un evento llamdo 'onvoiceschanged' 
      console.log(w.speechSynthesis)
      
      //👇 entonces para formar la lista de voces necesitamos ejecutar ese evento 'voiceschanged'
      speechSynthesis.addEventListener('voiceschanged', e => {
        
        //👇 y aqui dentro llamamos a w.speechSynthesis.getVoices(), eso nos devuelve la lista de voces
        console.log(w.speechSynthesis.getVoices())
        //👇 y aqui en nuestra variable de arreglo guardamos nuestra lista de voces
        voices = w.speechSynthesis.getVoices()
        
        //📄NOTA: recordemos q cada una de estas voces es un objeto, ahi viene la url donde esta la voz en la Pc, el nombre y algunos 
        //  otros parámetros. Entonces lo q tenemos q hacer es crear dinamicamente x cada una de estas voces de este arreglo, una
        //  option en un select. 👇Veamos como hacerlo con un forEach
        voices.forEach(voice => {
          const $option = d.createElement('option')
          $option.value = voice.name
          $option.textContent = `${voice.name} *** ${voice.lang}`
          $speechSelect.appendChild($option)
        })
      })
    })
    
    d.addEventListener('change', e => { //👈 esto es para q cada vez q cambiemos una option del select, cambiemos la voz
      // si el objeto q origina el evento es $speechSelect
      if(e.target === $speechSelect){
        // se va a ejecutar $speechgMessage en su propiedad voice, y en la variable arreglo voices buscamos (recordemos q en el
        // select cuando se ejecuta el evento change el e.target tiene un valor), entonces lo q haremos es buscar el nombre de la voz
        // con el valor q traiga el e.target, en este caso el option q se esta moviendo, y esa voz es la q vamos a asignar
        $speechgMessage.voice = voices.find(voice => voice.name === e.target.value)
        console.log($speechgMessage)
      }
    })
    
    d.addEventListener('click', e => { //👈 esto es para q cada vez q le demos click al botón, el Narrador lea el texto
      
      // Ahora sólo falta q cuando le demos click al botón, utlicemos el método q le permite a esta interfaz SpeechSynthesisUtterance
      //  hablar y hablaría con la voz seleccionada
      if(e.target === $speechBtn){
        // decimos q la API q contiene $speechgMessage (SpeechSynthesisUtterance) en su propiedad text va a guardar el texto q luego se va a hablar
        $speechgMessage.text = $speechTextarea.value
        // Ahora a la otra API SpeechSynthesis le decimos q hable
        w.speechSynthesis.speak($speechgMessage)
      }
      
    })
    
}

