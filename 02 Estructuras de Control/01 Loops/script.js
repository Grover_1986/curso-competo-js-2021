
let contador = 0

while(contador < 10){
     console.log('WHILE ' + contador++)
}

console.log('\n')


do{
     console.log('DO WHILE ' + contador)
     contador++
}while(contador < 10)

console.log('\n')


for(i = 0; i < 10; i++){
     console.log(`For ${i}`)
}

let arr = [10, 20, 30, 40, 50]

for(i = 0; i <= arr.length -1; i++)
     console.log(arr[i])
     
     
// El For In se usa para Objetos, recorre las propiedades del Objeto
const persona = {
     nombre:'Grover',
     apellido: 'Cristobal',
     edad: 34,
}
for(propiedad in persona){
     console.log(`Key: ${propiedad}  Value: ${persona[propiedad]}`)
}


// El For Of se usa para Arrays, recorre todos los elementos de un Objeto iterable como un Array
for(elements of arr){
     console.log(elements)
}

// tmb puede recorrer carácteres
let frase = 'Hola mundo!'
for(caracteres of frase){
     console.log(caracteres)
}
