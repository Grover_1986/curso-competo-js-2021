
// CADENAS DE TEXTO
let nombre = 'Grover'
let ape = 'Garrido'
let saludo = 'Hola mundo'
let lorem = "Mi verde tren me jala sobre una noche primaveral!"

console.log(
     nombre.length,
     ape.length,
     saludo.length,
     nombre.toUpperCase(),
     ape.toLowerCase(),
     lorem.includes('ja'), // da true o false si unas letras o palabras se encuentran
     lorem.includes('lorenzo'),
     lorem.trim(), // quita espacios de la derecha como tmb de la izquierda
     lorem.split(' ') // convierte un Cadena String a un Array por medio de una separación entre ''
)

// Replace()  ->  coge un texto y lo reemplaza por otro
console.log(nombre.replace('Grover','Elvis'))


// Substring()  ->  extrae caracteres desde indiceA hasta indiceB sin incluirlo, nos retorna un String   
console.log(ape.substring(0,4)) // devuelve Garr

// Slice()  ->  El índiceA es basado en cero en el cual empieza la extracción. El índiceB es basado en cero en el que termina la extracción. Si se omite, slice() extrae hasta el final de la cadena. Nos retorna una sección de la cadena
console.log(ape.slice(1,-1)) // devuelve arrid

// charAt()  ->   Los caracteres de una cadena se indexan de izquierda a derecha. El índice del primer caracter es 0
console.log(nombre.charAt(0)) // devuelve G


// indexOf()  ->  Los caracteres de una cadena se indexan de izquierda a derecha. El índice del primer carácter es 0, y el índice del último carácter de una cadena llamada nombreCadena es nombreCadena.length - 1. Nos devuelve un indice

// Sintaxis :  cadena.indexOf(valorBusqueda[, indiceDesde])

/**
 * valorBusqueda
     Una cadena que representa el valor de búsqueda.
 * indiceDesde
     La localización dentro de la cadena llamada desde la que empezará la búsqueda. Puede ser un entero entre 0 y la longitud de la cadena. El valor predeterminado es 0. 
*/
console.log(saludo.indexOf('o',2)) // devuelve 9

console.log(lorem.lastIndexOf('a'))

// startsWith()
console.log(saludo.startsWith('Hola')) // nos retorna un Boolean (false o true)
