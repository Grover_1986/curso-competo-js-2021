/**
 *! API REST

    *? API .- es un conjunto de rutinas q nos provee acceso a ciertas funcionalidades de un software en especifico.
        //? Ejm: las APIs q vimos para la manipulación del documento Html a tráves de JS más conocido como el DOM,
        //? otro Ejm: la API para acceder a la Geolocalización, para acceder a la cámara web, etc.

        //* Dentro del mundo de las APIs tenemos las APIs Nativas y las APIs Externas, veamos:
        
        //todo APIs Nativas .- es todo lo q el motor de JS de cada Navegador trae integrado para acceder al DOM, a la Cámara Web,
        //*todo     a la Batería, a los Puertos USB, a los Estados de Conexión, etc.
        
        //todo APIs Externas .- 
        //*  por Ejm: si tenemos una cuenta en GitHub podemos acceder a nuestros datos en formato JSON en la API de GitHub, eso no
        //*      es más q una URL como api.github.com/users/nameuser y nos daba toda la info de nuestra cuenta.
        //*  otro Ejm: el API de Facebook para obtener la info de un perfil, el API de Twitter para obtener los tweets de un usuario.
        //*  otro Ejm: WordPress tiene una API de tipo Rest en la cual podemos construir un Sitio Web con Html accediendo a la API
        //*     Rest de un sitio en WordPress y poder extraer Categorias, Entradas, Eiquetas, Articulos, Info de Usuarios y no tener
        //*      q depender del front de las plantillas de WordPress
        //todo La mayoría de APIs Externas te dan una documentación para saber como usarlas
        
    
    *? REST .- Significa Transferencia del Estado Representacional, y un Servicio REST es un conjunto de restricciones con las q po-
    *?   demos crear un estilo de arquitectura de software, digamos q hemos ido migrando del MVC a una arquitectura basados en REST.
    *?   REST se basa en las reglas y los estándares del protocolo HTTP
    
    
    *? CARACTERISITICAS API REST O REST FULL
    //*1️⃣ La relación Cliente Servidor está debilmente acoplados, eso significa q están separados, es decir al cliente no le inter-
    //*   esa los detalles de como se implementó el lado del Servidor y al Servidor no le interesa como el cliente vaya a utilizar
    //*   esta info para pintarla en los datos.
    
    //*2️⃣ Las API REST son sin Estado, es decir cada petición recibida por el Servidor se va a manejar de manera independiente lo q
    //*   evita q mantenga sesiones conectadas como para hacer varias peticiones al mismo tiempo.
    
    //*3️⃣ Debe ser cacheable, quiere decir q debe haber un sistema de almacenamiento en Caché para evitar repetitivas conexiones 
    //*   hacía un mismo recurso del Servidor, y esto pues va a impactar en el rendimiento de las aplicaciones.
    
    //*4️⃣ Hay q mantener una interfaz uniforme, esto significa una interfaz generica q permita la administración entre la interacc-
    //*   ión entre el Cliente y el Servidor de manera uniforme, es decir q para cada petición va a existir una URL única y con las
    //*   caracteristicas de como vamos a enviar la info, esto en el mundo de las API REST se conoce como EndPoint o los
    //*   Puntos Finales a los cuales voy acceder a cierto determinado recurso.
    
    //*5️⃣ Una API REST bien diseñada debe tener un sistema de capas, esto quiere decir q del lado del Servidor debe de haber varias
    //*   capas a la hora de inplementarla para estar previstos si más adelante ese desarrollo escala xq a lo mejor necesitamos más
    //*   caracteristicas. 
    
    
    //? LAS API REST SON EL NUEVO CRUD
    //TODO  OPERACIONES CRUD    PETICIONES REST
    //*     INSERT      👉      POST
    //*     SELECT      👉      GET
    //*     UPDATE      👉      PUT o PATCH 
    //*     DELETE      👉      DELETE
    
    *! NOTA:
    *?1️⃣Hasta ahora hemos estado utilizando la API de JSONPlaceholder q es una API Falsa, y aunque tiene los verbos GET, POST, PUT,
    *?  DELETE, es simulado; podemos simular hacer un Post, pero si deseamos hacer un INSERT de una publicación y luego un GET para 
    *?  obtener el nuevo Post junto con los q ya se tenía, como son APIs Falsas esto no nos permite. Sin embargo JSONPlaceholder 
    *?  tiene un proyecto JSON SERVER y si le damos click nos llevará a un GitHub con la documentación para poder instalarlo.
    
    *?2️⃣Usaremos la API de JSON SERVER q tmb es falsa para simular un Backend y aprender como hacer peticiones ahora si poder 
    *?  usando los CRUDs 
    
    *?3️⃣Para usar JSON SERVER tenemos q tener instalado Node.js
    
    *?4️⃣Usaremos clientes de APIs para hacer pruebas de APIs como Insomnia, tmb tenemos a Postman, pero usaremos Insomnia xq es
    *?  xq su interfaz es muy minimalista
    
        
 */