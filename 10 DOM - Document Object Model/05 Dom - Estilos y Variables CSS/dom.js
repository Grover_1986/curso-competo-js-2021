
// EN ESTA CLASE VEREMOS COMO INTERACTUAR CON LOS ESTILOS DE NUESTROS ELEMENTOS HTML
const $linkDOM = document.querySelector('.link-dom')
 
/* **📝 OBTENER LOS ESTILOS CSS ** */

//  👉 .style 
//  👉 .getAttribute() 
//  👉 .getComputedStyle()

console.log($linkDOM.style) //👈 el .style nos entrega un mapa donde estan todas las propiedades de CSS válidas
console.log($linkDOM.getAttribute('style')) //👈 esto nos devuelve con más exactitud los styles css que le estamos dando a $linkDOM
console.log($linkDOM.style.color)   //👈 obtiene el color
console.log(window.getComputedStyle($linkDOM))  //👈 window con .getComputedStyle() tmb nos puede mostrar las popiedades CSS, pero a éstas se les conoce como Computed Properties o Propiedades Dinámicas, es una parte de la WEB API q tienen los Navegadores; como argumento le damos un elemento.
console.log(getComputedStyle($linkDOM).getPropertyValue('color'))  //👈 no es necesario anteponer window xq es el objeto global y con .getPropertyValue() obtenemos el valor de una propiedad CSS cuando usamos getComputedStyle()



/* **📝 ESTABLECER LOS VALORES CSS ** */

//  👉 .style 
//  👉 .getAttribute() 
//  👉 .getComputedStyle()
 
$linkDOM.style.setProperty('text-decoration','none')
$linkDOM.style.setProperty('display','block')

$linkDOM.style.textAlign = 'center'
$linkDOM.style.padding = '1rem'
$linkDOM.style.width = '50%'
$linkDOM.style.marginLeft = 'auto'
$linkDOM.style.marginRight = 'auto'
$linkDOM.style.borderRadius = '.5rem'


 
/* **📝 VARIABLES CSS - CUSTOM PROPERTIES CSS ** */

//👇 Para acceder a las variables CSS crearemos una variable para el HTML y otra para el BODY
const $html = document.documentElement,
      $body = document.body
      
//👇 ahora crearemos variables para guardar a las variables CSS
let varYellowColor = getComputedStyle($html).getPropertyValue('--yellow-color'),
    varDarkColor = getComputedStyle($html).getPropertyValue('--dark-color')

console.log(varYellowColor , varDarkColor)

//👇 Aplicamos al Body
$body.style.backgroundColor = varDarkColor
$body.style.color=  varYellowColor

//👇 Ahora modificamos las variables CSS y las mostramos en el Body
$html.style.setProperty("--dark-color", "pink")  //👈 aquí actualizamos q pink es el nuevo color de la variable
varDarkColor = getComputedStyle($html).getPropertyValue("--dark-color")  //👈 aquí reestablecemos
$body.style.setProperty("background-color", varDarkColor)   //👈 aquí se confirma el nuevo estilo para el Background del Body (color pink)  