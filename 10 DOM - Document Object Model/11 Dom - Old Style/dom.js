/* 
    Hasta el momento solo hemos visto un método para agregar elementos dinámicos appendChild(), pero tenemos otros métodos q nos permiten reemplazar,
    o insertarlo al inicio, o insertarlo en una posición en particular, para ello tmb usaremos las propiedades del Dom Traversing para poder tener esa
    interacción. En esta clase veremos los métodos de antaño, el Old Style
*/
 
const $cards = document.querySelector('.cards'),
      $newCard = document.createElement('figure')

$newCard.innerHTML = `
    <img src="https://placeimg.com/200/200/any" alt="Any">
    <figcaption>Any</figcaption>
`
$newCard.classList.add('card')
 
//👇 Reemplazaremos la imagen de Arch con replaceNode(). 
// Empezamos escribiendo la referencia (el padre), y replaceChild() usa como 1er argumento el Nuevo Nodo y 2do argumento es el Nodo a reemplazar
$cards.replaceChild($newCard, $cards.children[2]) 

//👇 insertBefore() => inserta antes. Ahora insertamos el elemento $newCard antes del primer elemento hijo
$cards.insertBefore($newCard, $cards.firstElementChild) //👈 inserBefore() usa dos argumentos, el 1ro es el nuevo Nodo, y el 2do es el Nodo de referencia q va a servir para insertar el nuevo Nodo, antes de ese!

//👇 removeChild() => elimina un Nodo. Podemos usar las propiedades del Dom Traversing en el argumento
$cards.removeChild($cards.lastElementChild)

//👇 cloneNode() => con este método podemos clonar un Nodo, en este caso clonaremos toda la seccion de Cards ($cards)
const $cloneCards = $cards.cloneNode(true)  //👈 usa como argumento un Boolean, el true es para q clone todo su contenido, el false o vacío estaria clonando solo la estructura del Nodo Padre, es decir el section con la clase cards (.cards)
//👇 aquí lo agregamos al final
document.body.appendChild($cloneCards)
