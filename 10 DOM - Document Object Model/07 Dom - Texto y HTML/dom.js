/**
 * Texto y HTML
 * Ahora veremos como poder interactuar con el contenido tanto textual como el contenido Html de un elemento.
 */
const $whatIsDom = document.getElementById('que-es')
 
//👇 variable q usaremos para reemplazar
let text = `
    <p>
        El Modelo de Objetos del Documento (<b><i>DOM - Document Object Model</i></b>) es un API para documentos HTML y XML
    </p>
    <p>
        Éste provee una representación estructural del documento, permitiendo modificar su contenido y presentación visual mediante código JS
    </p>
    <p>
        <mark>El DOM no es parte de la especificación de JavaScript, es una API para los Navegadores</mark>
    </p>
`
// MODIFICAREMOS EL CONTENIDO DEL id "que-es" POR EL CONTENIDO DE LA VARIABLE text, PARA ESO TENEMOS VARIAS PROPIEDADES:
 
//👇1️⃣ innerText fue creada para Internet Explorer, no forma parte del estandar, pero si funciona en cualquier Navegador. innerText imprime las etiquetas Html como texto
$whatIsDom.innerText = text
 
//👇2️⃣ textContent sirve para agregar contenido textual a un elemento, es decir sólo texto, ésta si es una propiedad estandar, quita los tabuladores q pudiera haber en el texto, pero tmb lee las etiquetas Html como texto
$whatIsDom.textContent = text
 
//👇3️⃣ innerHTML, sirve para cuando deseamos que el contenido se renderize como código Html, éste tmb sirve para reemplazar o agregar sólo el contenido (incluido etiquetas, atributos) de un elemento padre, es decir sólo los descendientes
$whatIsDom.innerHTML = text

//👇4️⃣ outerHTML, si nos damos cuenta tenemos el <p id='que-es'> y dentro hay otros 3 <p>, pues esto en semantica de Html está mal, pero lo solucionamos con outerHTML, pues éste sirve para reemplazar el elemento del DOM (<p id='que-es'></p>) y todo sus descendientes, por el contenido de la variable text
$whatIsDom.outerHTML = text

//📝 NOTA:
//👉 Cuando necesitemos usar sólo texto utilizamos texContent
//👉 Cuando necesitemos usar sólo código Html utilizamos innerHTML
//👉 La propiedad innerHTML establece o devuelve el contenido HTML (HTML interno) de un elemento.
//👉 La propiedad outerHTML establece o devuelve el elemento HTML y todo su contenido, incluida la etiqueta de inicio, sus atributos y la etiqueta de finalización.
