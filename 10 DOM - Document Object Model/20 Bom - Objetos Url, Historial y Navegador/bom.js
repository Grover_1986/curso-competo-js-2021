
console.log('***************** Objeto URL (location) *******************')
// Es un objeto q tiene varias propiedades
console.log(location)
// Estas son las propiedades más importantes de dicho objeto:
console.log(location.origin) //👈 nos da exactamente la ruta en la cual se origina
console.log(location.protocol)  //👈 es el protocolo
console.log(location.host)  //👈 es el dominio o la Ip si es q no hay dominio
console.log(location.hostname)  //👈 es el dominio o la Ip si es q no hay dominio
console.log(location.port)  //👈 es el puerto
console.log(location.href)  //👈 agarra toda la url y hasta las anclas internas o externas, tmb los parámetros
console.log(location.hash)  //👈 esto detecta el valor de la URL q esta despues de un gatito #
console.log(location.search)   //👈 almacena la cadena despues del signo ?, es decir los parámetros
console.log(location.pathname)  //👈 es el archivo al cual estamos consultando
// console.log(location.reload())  //👈 recarga la página


console.log('***************** Objeto Historial (history) *******************')
console.log(history)
console.log(history.length) //👈 significa cuantas páginas hemos visitado, son las q puedo acceder hacia atrás o hacía adelante en la pestaña en la q me encuentro, guarda el almacenamiento del historial en el que me encuentro
console.log(history.state)  //👈 devuelve un valor que representa el estado en la parte superior de la pila del historial. Esta es una forma de ver el estado sin tener que esperar un popstateevento.  
// console.log(history.back(2)) //👈 este método hace que el navegador retroceda una página en el historial de la sesión. Tiene el mismo efecto que llamar history.go(-1). Pasandole valores, podemos ir de un punto a otro
// console.log(history.forward(1))  //👈 método hace que el navegador avance una página en el historial de la sesión. Tiene el mismo efecto que llamar history.go(1). Pasandole valores, podemos ir de un punto a otro
// console.log(history.go(-1)) //👈 significa una página antes
// console.log(history.go(1))  //👈 significa una página después


console.log('***************** Objeto Navegador (navigator) *******************')
console.log(navigator)
console.log(navigator.connection)  //👈 nos da cierta información de conexión del usuario
console.log(navigator.geolocation)  //👈 devuelve un objeto Geolocation que proporciona acceso web a la ubicación de un dispositivo. Esto permite ofrecer al sitio web o aplicación resultados personalizados basados en la ubicación del usuario.
console.log(navigator.mediaDevices) //👈 proporciona acceso a dispositivos de entrada de medios conectados, como cámaras y micrófonos, así como para compartir pantalla.
console.log(navigator.mimeTypes) //👈 son los tipos de formatos q depende del tipo de aplicación, soportan los Navegadores web
console.log(navigator.onLine)  //👈 el Navegador tiene eventos para detectar cuando el usuario pierde la conexión o la vuelve a recuperar
console.log(navigator.serviceWorker) //👈 es una API q nos ayuda hacer Progressive Web Apps, es decir a convertir
console.log(navigator.storage)  //👈 es el API de almacenamiento
console.log(navigator.usb)  //👈 es el dispositivo USB
console.log(navigator.userAgent)  //👈 esto nos da mucha información sobre el Navegador
 