/**
 * 📝DELEGACIÓN DE EVENTOS
 *👉 Es un mecanismo a través del cual evitamos asignar eventListeners a múltiples nodos específicos del DOM, asignando un event listener a
 solo un nodo padre que contiene el resto de estos nodos.
 *👉 La ventaja de usar delegación de eventos es que en el caso hipotético de tener n elementos con la clase btn, solo hemos registrado un
 eventListener para todos estos elementos, mientras que sin delegación de eventos debemos registrar eventListeners, es decir uno por cada nodo.
 
 Ejm: En un formulario q tenga 30 inputs y cada uno tenga validaciones, en lugar de trabajar el evento onchange por asi decirlo, en cada input, o tmb el evento blur, o el evento focus, éste evento en cuestión en lugar de asignarlo a esos 30 inputs, lo asiganamos al formulario como tal, e incluso lo podemos asignar al modo raíz q es el Document, y asi en vez de tener 30 eventListeners generados al evento click,
 tenemos un solo event listener generado al Document y simplemente detectamos cual es el elemento q va a desencadenar ese evento.
 
 Veamos:
 
 */  
const flujoEventos = (e) => {//👇 este this hace referencia al objeto window, xq aunque el q lo llama es document, éste cuelga del obj window
            console.log(`Hola ${this}, el click lo originó ${e.target.className}`)
}
 
//👇 el event listener lo asociamos al Document
document.addEventListener('click', (e) => {
    console.log('Hiciste click en: ', e.target)
    
    //👇 ahora buscaremos la coincidencia de algún selector con una condici onal, y dentro de esa condicional ejecutaremos la programación
    //  para cada elemento del DOM, q tengamos q asignar el evento click
    
    // matches().- este método busca un selector válido, devuelve un Boolean
    if(e.target.matches('.eventos-flujo a')){ //👈 decimos: 'Si el objeto q origino el evento tiene este selector'
        alert('Hellooo')
        e.preventDefault()
    }
    
    if(e.target.matches('.eventos-flujo div')){
        flujoEventos(e)
    }
})

 
    
//📝NOTA: 
/*1️⃣Esta delegación de eventos tmb nos va a ayudar a evitar la propagación, y entonces ya no tendriamos q estar evitando la propagación, xq como el evento esta asignado al nodo superior Document ya no necesitamos este trabajo de la propagación.
 *2️⃣Como el Document es el q tiene asignado el click y el Document es el nodo raíz de nuestro documento, pues ya no hay un elemento padre 
 después de él, y x eso ya no es necesario el manejo del stopPropaagation.
 *3️⃣Esta técnica de la delegación es muy importante usar, sobre todo si mandamos a solicitar ciertos datos a una API, y con esos datos generamos elementos Html dinámicos, y luego si a esos elementos tenemos q asignarle eventos, la única manera de hacerlo es x medio de la
 delegación de eventos xq si generamos eventListeners normalitos a elementos q aún no existen en el DOM, nos va marcar algún error en la 
 consola diciendo 'Que no se le puede asignar un eventListener a un elemento q todavia no carga.
 */
