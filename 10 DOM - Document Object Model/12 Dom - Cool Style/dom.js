/* 
** EXISTEN 3 MÉTODOS
.insertAdjacent... 👉 inserta de manera Adyacente (continuo)
    .insertAdjacentElement(position,el) 👉 agrega de manera Adyacente un elemento, es parecido al appendChild() o el insertBefore()
    .insertAdjacentHTML(position,html)  👉 agrega de manera Adyacente contenido Html, es parecido a un innerHTML
    .insertAdjacentText(position,text)  👉 agrega de manera Adyacente texto, es como un texContent

ESOS TRES 👆 MÉTODOS TIENEN ESTAS 👇 CUATRO POSICIONES
posiciones:
    beforebegin (hermano anterior)  👉 antes de un elemento
    afterbegin (primer hijo)  👉 el primer hijo de la referencia
    beforeend (ultimo hijo)   👉 el último hijo de la referencia
    afterend  (hermano siguiente)
*/
    

const $cards = document.querySelector('.cards'),
      $newCard = document.createElement('figure')

$newCard.innerHTML = `
    <img src="https://placeimg.com/200/200/any" alt="Any">
    <figcaption>Any</figcaption>
`
$newCard.classList.add('card')
 

// USAREMOS .insertAdjacentElement(position,el)

/**📝
 * Los .insertAdjacent tienen dos parámetros:
 * 1.- Posición
 * 2.- Nuevo nodo a insertar
 */

//👇 Insertamos un elemento Html, 'beforebegin' es posición anterior, es decir, hermano anterior
$cards.insertAdjacentElement('beforebegin', $newCard)

//👇 'beforeend' es posición de último hijo
$cards.insertAdjacentElement('beforeend', $newCard)

//👇 'afterbegin' es posición del primer hijo
$cards.insertAdjacentElement('afterbegin', $newCard)

//👇 'afterend' es posición de hermano siguiente
$cards.insertAdjacentElement('afterend', $newCard)
 
/**👀
 * Las mismas posiciones se usan para .insertAdjacentHTML
 */


// AHORA USAREMOS .insertAdjacentHTML(position,html)

//👇 creamos nuevas variables para usar .insertAdjacentHTML()
const $newCard1 = document.createElement('figure'),
$contentCard = `
    <img src="https://placeimg.com/200/200/animals" alt="Animals">
    <figcaption></figcaption>
`
//👇 le damos la clase .card
$newCard1.classList.add('card')
//👇 insertamos el Html $contentCard a $newCard1
$newCard1.insertAdjacentHTML('afterbegin',$contentCard)
//👇 insertamos dentro de figcaption el texto Animal
$newCard1.querySelector('figcaption').insertAdjacentText('afterend','Animal')
//👇 ahora insertamos el elemento $newCard1 como hermano sgte de $cards
$cards.insertAdjacentElement('afterend', $newCard1)


//👇 prepend() inserta como primer hijo 
$cards.prepend($newCard1)
//👇 append() inserta como último hijo 
$cards.append($newCard1)
//👇 before() lo inserta como hermano anterior
$cards.before($newCard1)
//👇 after() inserta como hermano posterior
$cards.after($newCard1)

//📝NOTA:
// Todos estos elementos son la forma MÁS moderna de agregar elementos en el DOM.