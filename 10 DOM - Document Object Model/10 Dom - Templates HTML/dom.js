// En la clases anteriores vimos como crear elementos dinamicamente con JS y aprovechar un nodo muy en particular que son los fragmentos q nos van ayudar a mejorar el rendimiento de nuestra aplicación y a no estar pegandole a cada iteración q le tengamos q hacer al DOM.

// Ahora veremos una etiqueta nueva q son los templates, esta etiqueta template es como un modelo a seguir en el cual estructuramos el contenido Html q querramos q con JS se convierta en dinámico, y esta es otra forma de poder interactuar con el DOM. 

// Entonces en esta clase veremos en combinación con los fragmentos del DOM, a utilizar la etiqueta Template.
 
//👇 como vamos agregar varias tarjetas al selector cards crearemos una variable para q apunte al padre
const $cards = document.querySelector('.cards'),
$template = document.getElementById('template-card').content, //👈 creamos una variable q apunte al contenido del template, mas no a él mismo
$fragment = document.createDocumentFragment(),   //👈 creamos un fragmento para no estarle pegando cada vez q se agrege una nueva tarjeta al Dom
$cardContent = [    //👈 aqui agregamos una tarjeta por cada categoria q tenemos en el sitio de donde sacamos las imágenes
    {
        title: 'Tecnología',
        img: 'https://placeimg.com/200/200/tech'
    },
    {
        title: 'Animales',
        img: 'https://placeimg.com/200/200/animals'
    },
    {
        title: 'Arquitectura',
        img: 'https://placeimg.com/200/200/arch'
    },
    {
        title: 'Personas',
        img: 'https://placeimg.com/200/200/people'
    },
    {
        title: 'Naturaleza',
        img: 'https://placeimg.com/200/200/nature'
    }
]

// Aqui por cada elemento q trae cardContent haremos lo sgte:
$cardContent.forEach(el => {
    // como ya tenemos toda la estructura Html en el Template, tomamos como referencia el Template y le decimos q dentro de ella busque las respectivas etiquetas
    $template.querySelector('img').setAttribute('alt', el.title)
    $template.querySelector('img').setAttribute('src', el.img)
    $template.querySelector('figcaption').textContent = el.title
    //👇 Ahora, aquí varía un poco, ps este template es único y si entonces usamos ese template para la 1ra tarjeta, ya no estaría disponible para una 2da o 3ra tarjeta, entonces lo q tenemos q hacer es clonar dicho Nodo xq ese nodo nomas es como referencia, por eso clonaremos la estructura, y como ya hicimos la asignación de los atributos y del contenido textual, entonces ahora crearemos una variable clone y con importNode() se puede clonar todo un Nodo del Documento
    let $clone = document.importNode($template, true)  //👈 el 1er Argumento es el elemento q nos basemos para clonar y el 2do Argumento es un Boolean, q si es true copia toda la estructura interna, pero si es false estaría copiando sólo la etiqueta template de apertura y la etiqueta template  de cierre
    $fragment.appendChild($clone)
}) 
 
//👇 aqui cuando termina el forEach, le agregamos el fragmento al elemento cards.
 $cards.appendChild($fragment)
 
 
// 📝NOTA:
/** Las etiquetas Template no se renderizan en el DOM, xq el objetivo en el q crearon esta etiqueta es para q sea un Modelo a seguir, es decir como en POO, q se tiene una Clase y a partir de ella generar más Objetos. Es decir a partir de un Template Modelo empezar a generar estructuras del DOM dinamicamente, como en este caso q serían las etiquetas figure.
 * 
 * Si juntamos la combinación de generar elementos dinamicamente con createElement() y con fragment, perfectamente los podemos combinar con los Templates y haríamos un código bastante eficiente a la hora de interactuar con el DOM.
 * Librerias como Vue.js y Svelt han popularizado los Templates, xq para generar el código Html dinámico, utilizan estos Templates. 
 * 
 */ 
