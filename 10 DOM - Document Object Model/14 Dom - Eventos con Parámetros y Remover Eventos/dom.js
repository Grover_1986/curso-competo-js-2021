/**
 * 📝EVENTOS CON PARÁMETROS
 *👉 Ahora veremos como remover Eventos y tmb que pasa cuando necesitamos pasar parámetros a la función manejadora del Evento.
 *👉 Recordemos q una funcion q se ejecuta en un Evento se le conoce como Event Handler o Manejador de Eventos
 *👉 Recordemos tmb q hay una regla importante, y es que cualquier función q se ejecute en un Evento sólo puede recibir como parámetro el evento en sí 'e' o la palabra reservada 'event', pero solamente puede recibir ese parámetro, no se le pueden pasar más parámetros a una función manejadora de eventos.
 *👉 Pero q pasa si tenemos la necesidad q una función se ejecute en un Evento y pasarle parámetros, xq justamente necesitamos trabajar con esos parámetros, pues si lo podemos hacer, y es como engañar un poquito al DOM del Navegador.
 
 Veamos:
 */  
const $eventoMultiple = document.getElementById('evento-multiple')

function saludar(nombre = 'Desconocid@'){
    alert(`Hola ${nombre}`)
    console.log(event)
}

//👇 esto nos devuelve 'Hola [object MouseEvent]' y no es lo q esperabamos, ps esto pasa xq no le podemos pasar parámetros
$eventoMultiple.addEventListener('click', saludar)
 
//👀 Si necesitas q una función se ejecute en un Evento, pero necesitas pasarle parámetros, entonces simplemente te lo envuelves en una arrow function o una función anónima y ya le podemos pasar los parámetros, xq quien va estar recibiendo el evento es la arrow function.
//👇 esta seria la solución, esta es la manera de como podriamos pasar parámetros a un Evento sin la limtante de los Manejadores de Eventos 
$eventoMultiple.addEventListener('click', () => {
    saludar()
    saludar('Grover')
})

/* **************************************************************************************************************************************** */

/**
 * 📝ELIMINAR EVENTOS DE UN ELEMENTO
 */
const $eventoRemover = document.getElementById('evento-remover')

//👇 esto nos dará error 
$eventoRemover.addEventListener('dblclick', (e) => {
    alert(`Removiendo el evento de tipo ${e.type}`)
    console.log(e)
    //👇 podriamos remover este evento aquí, pero nos dará error xq falta el 2° parámetro q es su función manejadora (EventTarget), necesita dos argumentos forzosamente y solamente esta presentando uno.
    $eventoRemover.removeEventListener('dblclick')
})

//👇 Para remover un Manejador de Eventos, es decir una función asociada a un Evento, ésta tiene q estar guardada en una función, puede ser una función declarada o una función expresada
const removerDobleClick = (e) => {
    alert(`Removiendo el evento de tipo ${e.type}`)
    console.log(e)
    //👉 El removeEventListener() nos va a pedir 2 parámetros;
    // 1° El evento a remover.
    // 2° La función manejadora asociada a ese Evento.
    $eventoRemover.removeEventListener('dblclick', removerDobleClick)
    $eventoRemover.disabled = true
}

//👇 Y aqui llamamos a nuestra función manejadora para remover el evento 'dblclick'
$eventoRemover.addEventListener('dblclick', removerDobleClick)

// ESTA ES LA MANERA CORRECTA DE REMOVER EVENTOS CON LOS MANEJADORES MÚLTIPLES




 