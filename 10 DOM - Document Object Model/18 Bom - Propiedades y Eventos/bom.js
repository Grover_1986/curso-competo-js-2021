/**📝BOM (Browser Object Model)
 * No es más q una serie de métodos y de Objetos que cuelgan del window, hasta el momento hemos estado manejando el DOM, pero sin embargo
    hay una serie de métodos, propiedades y eventos q es importante conocer, q hacen referencia a la ventana del Navegador, es decir el objeto winodow
*/
  
//👇 tamaño de ancho del viewport de nuestra ventana
// console.log(`Ancho Viewport: ${window.innerWidth}`)
// //👇 tamaño de altura del viewport de nuestra ventana
// console.log(`Altura Viewport: ${window.innerHeight}`)
// //👇 tamaño de ancho del Navegador
// console.log(`Ancho Navegador: ${window.outerWidth}`)
// //👇 tamaño de altura del Navegador
// console.log(`Altura Navegador: ${window.outerHeight}`)


/** EL EVENTO 'resize' **/
//👇 Ahora, con este evento del Objeto window, podemos hacer algo interesante, q cada vez q redimensionemos la ventana, podamos ver la
//  impresión en pantalla, de los elementos anteriores y tmb un console.clear antes para limpiar la consola. Veamos:
window.addEventListener('resize', (e) => {
    console.clear()
    console.log('========= Evento Resize =========')
    console.log(`Ancho Viewport: ${window.innerWidth}`)
    console.log(`Altura Viewport: ${window.innerHeight}`)
    console.log(`Ancho Navegador: ${window.outerWidth}`)
    console.log(`Altura Navegador: ${window.outerHeight}`)
    console.log(e)
})


/** EL EVENTO 'scroll' **/
//👇 Este evento trabaja con las barras de desplazamiento, tiene dos propiedades importantes, scrollY y scrollX, estas propiedades siempre
//  serán números positivos y son expresados en pixeles.
// scrollY.- es cuanto se aleja la barra de desplazamiento vertical del margen top    
// scrollX.- es cuanto se aleja la barra de desplazamiento horizontal del margen left    
window.addEventListener('scroll', (e) => {
    console.clear()
    console.log('========= Evento Scroll =========')
    console.log(`Scroll Horizontal: ${window.scrollX}`)
    console.log(`Scroll Vertical: ${window.scrollY}`)
    console.log(e)
})


/** EL EVENTO 'load' **/
//👇 Este evento se ejecuta cuando la ventana del Navegador haya terminado de cargar, nos ayuda a saber en q coordenada de la pantalla 
//  empieza a dibujarse la ventana del Navegador, para eso usamos las propiedades screenX y screenY.
//  screenX.- esta propiedad de solo lectura devuelve la distancia horizontal, en píxeles, del borde izquierdo del navegador al lado
//              izquierdo de la pantalla.
//  screenY.- esta propiedad de solo lectura devuelve la distancia vertical, en píxeles, del borde superior del navegador al borde
//              superior de la pantalla.
window.addEventListener('load', (e) => {
    console.log('========= Evento Load =========')
    console.log(`Coordenada Horizontal: ${window.screenX}`)
    console.log(`Coordenada Vertical: ${window.screenY}`)
    console.log(e)
})
 

/** EL EVENTO 'DOMContentLoaded' **/
// Funciona igual que el evento load, pero es mucho más rapido en la carga
document.addEventListener('DOMContentLoaded', (e) => {
    console.log('========= Evento DOMContentLoaded =========')
    console.log(`Coordenada Horizontal: ${window.screenX}`)
    console.log(`Coordenada Vertical: ${window.screenY}`)
    console.log(e)
})

  
//📝NOTA:
/*👉 Es mejor usar el evento 'DOMContentLoaded' sobre todo cuando estemos haciendo peticiones asíncronas, xq este evento se dispara cuando el
    documento Html ha sido cargado y parseado, es decir cuando el Navegador ya tiene todas las etiquetas desde Doctype hasta el cierre de Html perfectamente parseada en su arbol de elementos. 👀, pero DOMContentLoaded no va a esperar a q carguen las hojas de estilos, las imágenes, los scripts, etc, para q empiecen a funcionar.
 *👉 En cambio el evento 'load' se dispara y hasta q haya cargado todo desde Doctype hasta el cierre de Html, y aparte si se espera a que se 
    parseen las hojas de estilo, a que carguen los scripts, a que carguen las imágenes y cualquier otro contenido, etc.
 *👉 path.- indica cual es el camino para q el evento llegue al elemento q lo origina 
 *👉 timeStamp.- es el tiempo de carga expresado en milisegundos
 */

