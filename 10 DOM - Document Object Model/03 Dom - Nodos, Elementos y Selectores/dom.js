/**
 * 📖 NODOS 📖
 *👉 Hay diferentes tipos de Nodos, los comentarios son un nodo, las etiquetas son un nodo, los textos como los párrafos y los encabezados etc, tmb son un nodo
 *👉 Hay como 12 tipos de nodos, pero para la interaccion del HTML nos interesan los nodos de tipo elemento y los nodos de tipo texto
 */

//👇 ESTOS SON MÉTODOS QUE YA NO SUELEN UTILIZARSE, XQ SE HAN REEMPLAZADO POR MÉTODOS MÁS ÓPTIMOS
console.log(document.getElementsByTagName('li')) //👈 obtiene elementos por nombre de etiqueta en una Colección HTML
console.log(document.getElementsByClassName('card'))   //👈 obtiene elementos por nombre de clase en una Colección HTML
console.log(document.getElementsByName('nombre'))   //👈 obtiene elementos de un form en su atributo name por medio de un NodeList q es otro objeto parecido al arreglo pero q tiene q ver con el DOM

/*👀: 
    👉 querySelector() sólo trae el primer selector de tipo q le hayamos especificado en el documento Html
    👉 si deseamos traer todos los selectores de cierto tipo usamos querySelectorAll()
*/

//👆 👇 LOS TRES PRIMEROS DE ARRIBA HAN SIDO REEMPLAZADOS POR querySelector() y querySelectorAll()
console.log(document.querySelector('a'))  //👈 recibe un selector válido de CSS, ya sea un id, una clase, una etiqueta Html, tmb una etiqueta Html q esté dentro de cierto id o de cierta clase, en conclusión cuaqluier selector válido que no sea una pseudo-clase o un pseudo-elemento
console.log(document.querySelector('#menu li')) //👈 trae un sólo descendiente, es decir la primera li q se encuentran dentro de #menu
console.log(document.querySelectorAll('#menu li')) //👈 trae descendientes, es decir todas las li q se encuentran dentro de #menu
console.log(document.querySelectorAll('a'))  //👈 si deseamos traer todos los selectores de cierto tipo usamos querySelectorAll()
console.log(document.querySelectorAll('a').length)  //👈 obtiene la longitud
console.log(document.querySelectorAll('.card')[2])  //👈 podemos acceder a un elemento en particular de la coleccion NodeList
document.querySelectorAll('a').forEach(el => console.log(el)) //👈 pinta en la consola todos los selectores de tipo a
console.log(document.getElementById('menu'))  //👈 obtiene un único elemento por Id

//📓 NOTA: ✏️
//👉 Actualmente se usa  querySelector() , querySelectorAll() y getElementById()
//👉 getElementById() es mucho más rápido que querySelector()
//👉 querySelector() y querySelectorAll() devuelven un objeto NodeList (colección de nodos)
//👉 Aunque NodeList no es un Array, es posible iterar sobre él utilizando forEach(). También puede convertirse a un Array usando Array.from.
//👉 getElementsByTagName() y getElementsByClassName() devuelven una Colección HTML (HTMLCollection)