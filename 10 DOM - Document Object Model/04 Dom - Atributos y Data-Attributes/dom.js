// AHORA VEREMOS COMO PODER TRABAJAR CON LOS ATRIBUTOS, A PARTIR DE HTML5, EL STANDAR NOS PERMITE CREAR NUESTROS PROPIOS ATRIBUTOS (data-attribute)

//👇 Tenemos 2 maneras de interactuar o de mandar a llamar a los Atributos y tmb de establecerle valores
console.log(document.documentElement.lang)
console.log(document.documentElement.getAttribute('lang'))


//👇 Obtenemos el atributo href de estas 2 maneras
console.log(document.querySelector('.link-dom').href)  //👈 nos devuelve toda la URL de Live Server
console.log(document.querySelector('.link-dom').getAttribute('href')) //👈 esta manera es más exacta xq sólo nos trae el valor del href 'dom.html'


//👇 Editamos y agregamos un nuevo valor a los atributos
document.documentElement.lang = 'es'  //👈 asigna en el atributo lang de la pestaña Elements
console.log(document.documentElement.lang)  //👈 y ahora se visualiza en la consola
document.documentElement.setAttribute('lang','es-PE') //👈 vemos el cambio en la pestaña Elements
console.log(document.documentElement.lang)  //👈 y ahora se visualiza en la consola


//👇 Ahora guardaremos nuestros elementos del DOM en variables y con el signo $, para saber q son variables del DOM
//👇 Usaremos const para guardar nuestras variables del DOM, xq no importa si dinámicamente con JS le agregamos estilos, le cambiamos atributos, o le hacemos algo, pero va a seguir siendo el mismo elemento del DOM a lo largo de la aplicación
const $linkDOM = document.querySelector('.link-dom')
$linkDOM.setAttribute('target','_blank') //👈 abrimos el link en una nueva pestaña del navegador    
$linkDOM.setAttribute('rel','noopener')  //👈 esto es para que no haya una dependencia entre la ventana q estamos abriendo y la ventana origen
$linkDOM.setAttribute('href','https://facebook.com/elvis.grover')  //👈 podemos tmb modificar un atributo o agregar un atributp


//👇 Eliminar Atributos
//👇 tmb tenemos hasAttribute() para verificar si el atributo existe y removeAttribute() para elminarlo
console.log($linkDOM.hasAttribute('rel')) //👈 devuelve true
$linkDOM.removeAttribute('rel')
console.log($linkDOM.hasAttribute('rel')) //👈 ahora devuelve false xq ya se eliminó


//👇 Ahora veremos los Data-Attributes
console.log($linkDOM.getAttribute('data-description'))  //👈 obtenemos Document Object Model
console.log($linkDOM.dataset) //👈 todos los data-attributes se guardan en un Mapa a manera de colección, en un dataset muy similar a los objetos. Cada data-attribute se va guardando en una propiedad de ese Mapa


//👇 Modificamos un data-attribute, ya sea con setAttribute o con la anotación del punto
console.log($linkDOM.dataset.description)  //👈 obtenemos Document Object Model
$linkDOM.setAttribute('data-description','Modelo de Objeto del Documento')   //👈 aquí le damos un nuevo valor en Elements
console.log($linkDOM.dataset.description)   //👈 y aqui mostramos ese cambio en la consola
$linkDOM.dataset.description = 'Sigueme en mi canal de youtube' //👈 así seria darle un nuevo valor con la anotacón del punto
console.log($linkDOM.dataset.description)  //👈 y aqui lo mostramos por consola


//👇 Válidamos y eliminamos los data-attribute
console.log($linkDOM.hasAttribute('data-description')) //👈 nos devuelve true xq si existe
$linkDOM.removeAttribute('data-description')   //👈 aquí eliminamos el data-attribute
console.log($linkDOM.hasAttribute('data-description')) //👈 nos devuelve false xq lo eliminamos



