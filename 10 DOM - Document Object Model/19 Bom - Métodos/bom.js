
// window.alert('ALerta')
// window.confirm('Confirmación') // devuelve true si es ACEPTAR o false si es CANCELAR
// window.prompt('Mensaje')
  
const $btnAbrir = document.getElementById('abrir-ventana'),
      $btnCerrar = document.getElementById('cerrar-ventana'),
      $btnImprimir = document.getElementById('imprimir-ventana')

let ventana

//👇 Abrimos una ventana 
$btnAbrir.addEventListener('click', (e) => {
    ventana = window.open('https://crisbalti.com')
})
 
//👇 Cerramos una ventana 
$btnCerrar.addEventListener('click', (e) => {
    // window.close('https://crisbalti.com') //👈 esto cierra la misma ventana en q estamos, pero no la que abrimos
    //👇 para q cierre la ventana q abrimos tenemos q crear una variable, llamarla y con el método close(), cerrará.
    ventana.close()
})

//👇 Imprimimos un documento
$btnImprimir.addEventListener('click', (e) => {
    window.print()
    // si deseamos dar estilos a nuestra impresión para cuando el usuario imprima ese documento web, podemos usar una hoja de estilos y ahi usar @media print
})

 
//📝NOTA:
//👉 El BOM (Browser Object Model) no es un estandar, digamos q habrán ciertas variantes de un Navegador a otro.  
//👉 Todo lo q cuelga del Objeto window, no es necesario anteponerle esa palabra window.