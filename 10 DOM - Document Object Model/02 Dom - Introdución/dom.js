console.log("*************** Elementos del Documento ****************")

//👇 El objeto global de JavaScript en los Navegadores es Window, y de window cuelgan todas estas APIs del Navegador.
console.log(window.document)  //👈 particularmente ahora trabajaremos con el DOM, por eso manejaremos document ya q de él cuelga todo el Html
console.log(document)  //👈 no es necesario anteponerle la palabra window, esto devuelve todo el documento Html con su Doctype
console.log(document.head)  //👈 esto nos devuelve toda la cabecera del Html
console.log(document.body)  //👈 esto nos devuelve todo el Body
console.log(document.html)  //👈 esto nos devuelve undefined xq así no se llama a todo el Html
console.log(document.documentElement)  //👈 esto si devuelve todo el Html menos el Doctype
console.log(document.doctype)  //👈 esto devuelve sólo el Doctype
console.log(document.charset)  //👈 esto nos devuelve el juego de caracteres UTF
console.log(document.title) //👈 accedemos al título de la página
console.log(document.links) //👈 devuelve todos los links en un HTMLCollection (colección de Html), OJO: aunque el HTMLCollection tiene corchetes[] y tiene la propiedad length, no es un arreglo, y x tanto no se le comparte todos los métodos de los arreglos, por ejm si queremos aplicar un map() a esa colección Html, 1ro tenemos q guardar la colección en un arreglo, pero si queremos aplicar un forEach si se puede, xq si comparten ese método
console.log(document.images) //👈 devuelve todas las imágenes en un HTMLCollection (colección de Html)
console.log(document.forms)  //👈 devuelve todos los formularios en un HTMLCollection (colección de Html)
console.log(document.styleSheets)   //👈 devuelve una lista de objetos de tipo stylesheet para las hojas de estilo CSS que están específicamente enlazadas o contenidas en el documento.
console.log(document.scripts )  //👈 devuelve todos los scripts JS en un HTMLCollection (colección de Html), nos muestra 2 script, el script del DOM y el script del Live Server
console.log(document.getSelection().toString())  //👈 cuando seleccionamos algo, tmb lo podemos mapear en JS y para poderlo mostrar a cadena de texto hay q convertirlo con tostring()

//👇 para ver el funcionamiento del console.log(document.getSelection().toString()) meteremos ese código dentro de un SetTimeout de 3 segundos, esto es para tener un tiempo de tres segundos en seleccionar algo y despues de los 3 segundos, se imprima en la consola lo seleccionado
setTimeout(() => {
    console.log(document.getSelection().toString())
},3000)

document.write('<h1>Hola bienvenidos a mi web!</h1>') //👈 esto nos permite escribir en el documento pero se va a mostrar en la última parte del documento, y en la pestaña Elements se va a situar después del script en q estamos trabajando y antes del cierre del body, el segundo script es del live server  