/**
 *📝 STOPPROPAGATION y PREVENTDEFAULT
 *👉 En esta ocasión veremos como detener esa Propagación de Eventos q vimos en la clase anterior, xq habrán veces donde no vamos a requerir q nuestro evento se propague hacía los elementos hijos o padres, dependiendo de la fase q estemos trabajando (Burbuja o Captura) y entonces se ejecute sólo una vez la programación de nuestra función manejadora.
 
 *👉 Por otra parte habrán veces donde ciertos elementos del DOM, tienen comportamientos o eventos ya por default, por Ejm: 
    1) El Boton Submit de un Formulario, pues si éste esta dentro de un formulario y lo presionamos, ese formulario se procesa.
    2) El Scroll, por ejm cuando estamos controlando el scroll de las barras de desplazamiento, ya sea con las flechas del cursor o con la ruedita del mouse, pues ese es el comportamiento por default q tienen las teclas de arriba y abajo o de izquierda y derecha en el caso de la bara de despalazamiento horizontal, y la rueda del mouse.
    3) Los enlaces, cuando se les da click nos van a llevar al contenido q tenga ese enlace en su propiedad href.
    
 *👉 Imaginemos q mandamos un formulario y en lugar de q se procese de manera convencional, nosotros lo q vamos hacer con JS es solicitar una petición asíncrona vía Ajax y esa petición Ajax va, consulta una API, consulta una BD y obviamente q va a tardar y cuando éste lista la respuesta nos va a responder; es x eso q tenemos q desactivar el comportamiento de q el formulario se procese de manera automatica.
 
 Veamos un ejemplo:
 */  

//📝 STOPPROPAGATION 
// con el método stopPropagation() podemos detener la propagación q teniamos en el flujo de eventos
const $eventsFlujo = document.querySelectorAll('.eventos-flujo  div')

function flujoEventos(e){
    console.log(`Hola te saluda ${this.className}, el click lo originó ${e.target.className}`)
    e.stopPropagation() //👈 con el método stopPropagation() podemos detener la propagación q teniamos en el flujo de eventos
}
 
$eventsFlujo.forEach(div => {
    div.addEventListener('click', flujoEventos)
})


//📝 PREVENTDEFAULT
// lo q hace preventDefault() es cancelar la acción por defecto de un elemento del DOM
const $linkEventos = document.querySelector('.eventos-flujo a')

$linkEventos.addEventListener('click', e => {
    alert('Hola soy Grover')
    e.preventDefault()  //👈 con el método preventDefault() podemos cancelar la acción por defecto de un elemento del DOM, como x ejemplo aquí, este $linkEventos en su atributo href ya no se ejecutará
    e.stopPropagation()
})

