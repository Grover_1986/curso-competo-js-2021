/**
 ***** 📝CREANDO ELEMENTOS 📝*****
 * Hasta el momento hemos aprendido a manipular nuestras etiquetas Html,
 * pero ahora crearemos nuestras etiquetas Html de manera dinámica a traves de JavaScript
 */

/********** 1ER MODO **********/
// Crearemos un figure adicional a traves de JS
const $figure = document.createElement('figure'),
      $img = document.createElement('img'),
      $figcaption = document.createElement('figcaption'),
      $figcaptionText = document.createTextNode('Animals')  //👈 creamos un Nodo de texto para la etiqueta figcaption,
      $cards = document.querySelector('.cards'),
      $figure2 = document.createElement('figure')
    // 👆 hasta aqui podriamos decir que ya estan creadas nuestras etiquetas
     
//👇 aqui les establecemos sus respectivos atributos 
$img.setAttribute('src','https://placeimg.com/200/200/animals')
$img.setAttribute('alt','Animal')
$figure.classList.add('card')

//👇 sin embargo aún no estan incorporadas en el árbol del DOM, para eso necesitamos irlas agregando
$figcaption.appendChild($figcaptionText)
$figure.appendChild($img)
$figure.appendChild($figcaption)
$cards.appendChild($figure)
 


 /********** 2DO MODO **********/
 $figure2.innerHTML = `
    <img src="https://placeimg.com/200/200/nature" alt="">
    <figcaption>Nature</figcaption>
 `
 $figure2.classList.add('card')
 $cards.appendChild($figure2)
 
 
 
 /**
 ***** 📝NOTA:
 * A veces tendremos la necesidad de consultar una Base de Datos o tmb de una API que nos esta regresando un JSON, y tendremos varios elementos q dinamicamente queremos agregar. Veamos un ejemplo de esto con un arreglo de estaciones.
 */

 /********** 1ER MODO **********/
const estaciones = ['Primavera','Verano','Otoño','Invierno'],
      $ul = document.createElement('ul')
      
document.write('<h3>Estaciones del Año</h3>')
 
//👇 Crearemos dinamicamente las li para la ul, entonces para empezar hacer eso, agregamos la ul dentro de su elemento padre Body
document.body.appendChild($ul)

//👇 recorremos el arreglo de estaciones
estaciones.forEach(el => {
   //👇 Creamos dinamicamente las li para la ul
    const $li = document.createElement('li')
    $li.textContent = el
    $ul.appendChild($li)
})

 
 /********** 2DO MODO **********/
const continentes = ['África','América','Asia','Europa','Oceania'],
      $ul2 = document.createElement('ul')

document.write('<h3>Continentes del Mundo</h3>')
document.body.appendChild($ul2)
$ul2.innerHTML = '' //👈 inicializamos como cadena vacía por si hay algún problema con algun Navegador viejo
continentes.forEach((el) => $ul2.innerHTML += `<li>${el}</li>`) //👈 si en vez de += ponemos = nos devolverá Oceania xq es el último elemento recorrido del arreglo, x eso tenemos q concatenarlo con += para q agarre a todos los elementos
  

//📝NOTA:
/*👉 Todo lo que hemos hecho esta muy bien xq ahorita son 5 o 4 elementos, pero imaginemos que tenemos q renderizar la vista de muchos elementos, asi como tmb una cantidad inimaginable de productos y el estar insertando con la técnica de innerHTML o con la técnica de createElement, que a cada paso del forEach, por cada elemento le estamos haciendo una inserción al DOM y las inserciones al DOM son las operaciones más demandantes de la problematica del desarrollo frontend y aunque estamos usando innerHTML, por cada recorrido estamos modificando el contenido Html de ese elemento, es así q por cada iteración del ciclo le estamos pegando al DOM, y si tenemos pocos elementos no pasa nada, pero si tenemos un renderizado de ciento de productos como en un ecommerce, pues a lo mejor esas operaciones van hacer q la PC q esta cargando la aplicación aumente el rendimiento y los recursos y bueno tmb considerando la conexión a intenet del usuario puede ser q eso lentee la aplicación,
 👉 Asi que en lugar de q cada iteración le esté pegando al DOM, nosotros en el DOM podemos crear fragmentos dinámicos, y a ese fragmento del DOM le podemos estar iterando un nuevo elemento y en lugar de estarle pegando al DOM, le pegamos a ese fragmento del DOM q esta en memoria y una vez q estan cargados todos los elementos en ese fragmento, entonces ya sólo hacemos una sóla inserción al DOM con los 5, con los 100, o con los miles de registros q nuestra aplicación haya capturado y eso mejora mucho el rendimiento.
 
 Veamos un ejemplo: 
 */  

 
/**
 **** 📝CREANDO FRAGMENTO 📝*****
 */
const meses = [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Setiembre',
    'Octubre',
    'Noviembre',
    'Diciembre'
],
$ul3 = document.createElement('ul'),
//👇 aqui estamos creando el fragmento 
$fragment = document.createDocumentFragment()

//👇 como estamos usando fragmentos del DOM no podemos usar el innerHTML, sino q vamos a crear elementos Nodos con createElement, y esa será la forma más óptima y adecuada
meses.forEach(el => {
    const $li = document.createElement('li')
    $li.textContent = el
    $fragment.appendChild($li)
})

document.write('<h3>Meses del Año</h3>')
$ul3.appendChild($fragment)
document.body.appendChild($ul3)

