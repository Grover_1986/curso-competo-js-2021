/**
 * *** CLASES CSS ***
 * Hasta el momento ya hemos visto a usar selectores, aplicar estilos y a obtener los atributos de una etiqueta Html, 
 * sin embargo aún no hemos visto de como acceder a las clases  
 */
 
//👉 Podemos acceder a la cadena de texto que se tiene escrita en el atributo class, a traves de una propiedad q se llama className o,
//👉 podemos acceder a un Objeto especial del Navegador q se llama DOMTokenList, donde es como si fuera un arreglo donde en cada posición nos va a devolver cada una de las clases q tenga el elemento en custión.
 

//👇 traemos el primer card q se encuentre en el árbol del documento
const $card = document.querySelector('.card')
console.log($card)

//👇 aqui accedemos a la cadena de texto del atributo class
console.log($card.className) //👉 => card

//👇 esto nos devuelve un DOMTokenList q pareciera un arreglo, pero es un Objeto especial del Navegador, a traves de classList() tenemos una serie de métodos q nos van ayudar, por Ejm: a evaluar si un elemento tiene una clase en particular. Veamos
console.log($card.classList)

//👇 Se evalúa si es q hay una clase con .contains(), ésta devuelve un boolean (true o false) en caso de tener o no la clase q le indiquemos
console.log($card.classList.contains('rotate-45')) //👉 => false

//👇 Agregamos clases con .add()
$card.classList.add('rotate-45')
console.log($card.classList.contains('rotate-45')) //👉 => ahora si retorna true
console.log($card.className) //👉 => ahora nos retorna, card rotate-45
console.log($card.classList) //👉 notamos q el DOMTokenList ahora tiene 2 elementos

//👇 Eliminamos clases con .remove()
$card.classList.remove('rotate-45')
console.log($card.classList.contains('rotate-45')) //👉 ahora es false

//👇 .toggle(), este método funciona como interruptor o palanca. Si el elemento tiene la clase se la quita, pero si no la tiene se la agrega
$card.classList.toggle('rotate-45') //👉 aqui se le agrega la clase, xq no tenia la clase

//👇 Reemplazamos clase con .replace(), el 1er parámetro es la clase a reemplazar y el 2do es la nueva clase
$card.classList.replace('rotate-45','rotate-135')

//👇 Agregando varias clases
$card.classList.add('opacity-80','sepia')

//👇 Eliminando varias clases
$card.classList.remove('opacity-80','sepia')

//👇 Agregamos varias clases si no las tiene
$card.classList.toggle('opacity-80','sepia')
