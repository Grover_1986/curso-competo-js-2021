/**
 *📝DOM Traversing.- No es más q una serie de propiedades q nos da el API del DOM para poder recorrer diferentes elementos, tomando como referencia un nodo.
 Ahora, es muy importante saber q todos los métodos q veremos en esta clase son para los elementos, recordemos q en la clase 61 hablamos de varios tipos de Nodos, unos 12 tipos exactamente: Nodos de Elementos, Nodos de Texto, Nodos de Comentarios, Nodos de Fragmentos, etc.
 Asi q este DOM Traversing lo usaremos utilizando las propiedades q nos sirven para recorrer los elementos
 */ 
 
//👇 Vamos a recorrer los elementos del DOM a traves de .cards
const $cards = document.querySelector('.cards')
console.log($cards)

//👇 Referenciamos a los hijos de .cards
console.log($cards.childNodes)

//👇 Referenciamos a un hijo en particular
console.log($cards.children[2])

//👇 Referenciamos al padre de .cards
console.log($cards.parentElement)

//👇 Referenciamos al primer elemento hijo de .cards
console.log($cards.firstElementChild)

//👇 Referenciamos al último elemento hijo de .cards
console.log($cards.lastElementChild)

//👇 Referenciamos al hermano q esta antes de .cards
console.log($cards.previousElementSibling)

//👇 Referenciamos al hermano q esta después de .cards
console.log($cards.nextElementSibling)

//👇 closest() es un método q busca el padre más cercano del tipo de selector q nosotros le demos
console.log($cards.closest('div')) //👉 devuelve null
console.log($cards.closest('body')) //👉 devuelve body

//👇 aquí estamos referenciando al cuarto hijo de .cards y estamos llamando a la section ancestral más cercana q tiene
console.log($cards.children[3].closest('section'))

//📝NOTA:
/*👉 Cuando usamos $cards.childNodes, $cards.firstChild, .lastChild, .previousSibling, .nextSibling, hacemos referencia al primer Nodo en nuestro código Html, que sería el Enter y al desplegar el text en la consola nos da el signo del Enter y todo eso se nos devuelve en un NodeList
 */ 