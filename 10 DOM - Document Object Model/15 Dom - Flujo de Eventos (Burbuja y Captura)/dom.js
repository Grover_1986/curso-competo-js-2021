/**
 * 📝FLUJO DE EVENTOS
 *👉 Habiamos dicho que el addEventListener() tiene un tercer parámetro y es justamente el que veremos ahora
 *👉 Principalmente hay dos maneras de trabajar en como se va propagando el evento
 *👉 Una vez q el evento se origina tiene una propagación a lo largo del árbol del DOM, por defecto esa propagación se va dando del elemento más interno al elemento más externo, q en este caso es el Document o tmb Window, y esa fase se llama de Burbuja. Por defecto este es el esquema y el modelo q los Navegadores soportan.
 
    Veamos:
 
 */
 
// Creamos una variable del DOM q nos capturen esas tres div q están en la section .evento-flujos
const $divsEventos = document.querySelectorAll('.eventos-flujo div')
console.log($divsEventos)  //👉 NodeList(3) [div.uno, div.dos, div.tres]

// En la clase anterior sólo estuvimos asignando eventos de manera individual a cada uno de los botones, pero imaginemos q en una interfaz dinámica esos botones (botonera) se forman a partir de un catalogo q tengamos en la Base de Datos, entonces tenemos q ir a consultar la Base de Datos, tenemos q imprimir un boton por cada registro q venga de la Base de Datos y a ese boton asignarle dinámicamente un evento. Entonces para eso tenemos q asignarle dinámicamente el evento a todos los elementos, para esto nos ayudaremos del método forEach(), Veamos:
function flujoEventos(){
    // console.log('Hola')
}

$divsEventos.forEach(div => {
    div.addEventListener('click', flujoEventos)
})
//👆 Fijemonos q cuando le damos click al div N°3, internamente la div 3 está dentro de la 2 y de la 1, y como los tres elementos tienen asignado el evento click, justamente ahí vemos la propagación del evento, x eso tenemos un console.log de 3 veces, y cuando damos click en N°2, ese 2 está dentro de 1, entonces ahí la propagación sería entre 2 y 1, por eso de 3 pasamos a 5 console logs xq se va sumando 2 click más, y cuando damos click a N°1, ya no hay más div; y ahí nos damos cuenta q están los 6 console logs
 
//👇 Veamos otro ejemplo de Propgación de Eventos: 
// esta función se esta ejecutando como Manejador del Evento 
function flujoEventos1(e){      //👇 por eso este this hace referencia a la div en cuestión
    console.log(`Hola te saluda ${this.className}, el click lo originó ${e.target.className}`)
}

//📝NOTA: 

/** Fase de Burbuja **/ 
//👉 Ahora, la función addEventListener() tiene un tercer parámetro opcional, q es un Boolean, por defecto este parámetro es false, y cuando esta en false significa q estamos en Fase de Burbuja (el flujo del evento se propaga del más interno al más externo dentro del árbol del DOM).

/** Fase de Captura **/ 
//👉 Pero si queremos el modelo contrario, q es la Fase de Captura (va capturando la Burbuja de los eventos), éste va del elemento más externo al elemento más interno.


/** Fase de Burbuja **/ 
$divsEventos.forEach(div => {                           
    // div.addEventListener('click', flujoEventos1, false) // Hola te saluda tres, el click lo originó tres
                                                        // Hola te saluda dos, el click lo originó tres
                                                        // Hola te saluda uno, el click lo originó tres
})


/** Fase de Captura **/ 
$divsEventos.forEach(div => {
    // div.addEventListener('click', flujoEventos1, true)  // Hola te saluda uno, el click lo originó tres
                                                        // Hola te saluda dos, el click lo originó tres
                                                        // Hola te saluda tres, el click lo originó tres
})
 
// Adicionalmente en este 3er parámetro, tmb le podemos especificar algunas cosas adicionales, pues le podemos pasar un Objeto
// Veamos:
$divsEventos.forEach(div => {
    div.addEventListener('click', flujoEventos1, {
        capture: true,   //👈 el parámetro capture hace referencia a si es fase de Burbuja (false) o de Captura (true)
        once: true      //👈 esto significa q nuestro evento se va a ejecutar una sóla vez (true), o varias veces (false)
        // no importa el objeto q originó el evento, solamente se va a ejecutar una sóla vez
    })
})


/**👀 
 * Esta fase de Captura y Burbuja la podemos omitir si delegamos los eventos a un elemento Padre más externo y eso tmb ayuda a mejorar el rendimiento de la memoria y los recursos q nuestra aplicación ocupe en el Navegador y x ende en la PC o dispositivo donde se esté desplegando la aplicacón.
*/
